### What is this repository for? ###

* NLPIndexer is a tool to manage NLP processing and make queries over syntactic patterns in order to extract information.
* v1.3.1 - To download last version go to https://bitbucket.org/fogallego/nlpindexer/downloads and select "Tags" tab.

### How do I get set up? ### TODO

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Fernando O. Gallego: [fogallego@us.es](mailto:fogallego@us.es)