package es.us.lsi.fogallego.nlpindexer.dao;

import es.us.lsi.fogallego.nlpindexer.model.dataset.DataSet;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface DataSetDao extends MongoRepository<DataSet, String> {

    public DataSet findById(String idDataSet);

//    public DataSet findByDatasetConfiguration_Output_Name(String name);

}
