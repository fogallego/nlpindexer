package es.us.lsi.fogallego.nlpindexer.validation.measures;

import es.us.lsi.fogallego.nlpindexer.model.validation.ConfusionMatrix;
import es.us.lsi.fogallego.nlpindexer.validation.AbstractValidationMeasure;

public class Precision extends AbstractValidationMeasure {

    public static final String PRECISION = "Precision";

    @Override
    protected String measureName() {
        return PRECISION;
    }

    @Override
    protected Double calculateWithMicroAverage(ConfusionMatrix confusionMatrix) {

        double value;

        if (confusionMatrix.getTrueNegative().signum() == 0 && confusionMatrix.getFalsePositive() == 0) {
            value = 1;
        } else {
            value = ((double) confusionMatrix.getTruePositive()) /
                    (confusionMatrix.getTruePositive() + confusionMatrix.getFalsePositive());
        }

        return value;
    }
}
