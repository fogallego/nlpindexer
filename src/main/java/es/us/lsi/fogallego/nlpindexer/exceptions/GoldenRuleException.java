package es.us.lsi.fogallego.nlpindexer.exceptions;

public class GoldenRuleException extends ApiException {


    public GoldenRuleException() {
        super();
    }

    /**
     * Instantiates a new GoldenRuleException api exception.
     *
     * @param message the message
     * @param responseStatusCode the response status ode
     */
    public GoldenRuleException(String message, int responseStatusCode) {
        super(message,responseStatusCode);
    }

    public GoldenRuleException(Exception e) {
        super(e);
    }
}