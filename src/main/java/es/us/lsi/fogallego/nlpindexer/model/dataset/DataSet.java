package es.us.lsi.fogallego.nlpindexer.model.dataset;

import es.us.lsi.fogallego.nlpindexer.dto.search.SearchParamsDTO;
import es.us.lsi.fogallego.nlpindexer.model.enums.EnumDataSetState;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "dataset")
public class DataSet {

    @Id
    private String id;
    private SearchParamsDTO datasetConfiguration;
    @CreatedDate
    private DateTime creationDate;
    private EnumDataSetState state;
    private Float timeToComplete;
    private Long numberOfDataSetElements;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SearchParamsDTO getDatasetConfiguration() {
        return datasetConfiguration;
    }

    public void setDatasetConfiguration(SearchParamsDTO datasetConfiguration) {
        this.datasetConfiguration = datasetConfiguration;
    }

    public DateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(DateTime creationDate) {
        this.creationDate = creationDate;
    }

    public EnumDataSetState getState() {
        return state;
    }

    public void setState(EnumDataSetState state) {
        this.state = state;
    }

    public Float getTimeToComplete() {
        return timeToComplete;
    }

    public void setTimeToComplete(Float timeToComplete) {
        this.timeToComplete = timeToComplete;
    }

    public Long getNumberOfDataSetElements() {
        return numberOfDataSetElements;
    }

    public void setNumberOfDataSetElements(Long numberOfDataSetElements) {
        this.numberOfDataSetElements = numberOfDataSetElements;
    }
}
