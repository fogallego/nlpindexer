package es.us.lsi.fogallego.nlpindexer.dto.search;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import es.us.lsi.fogallego.nlpindexer.dto.search.output.SearchOutputDatasetDTO;
import es.us.lsi.fogallego.nlpindexer.dto.search.output.SearchOutputFileDTO;
import es.us.lsi.fogallego.nlpindexer.dto.search.output.SearchOutputResponseDTO;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = SearchOutputFileDTO.class, name = "file"),
        @JsonSubTypes.Type(value = SearchOutputDatasetDTO.class, name = "dataset"),
        @JsonSubTypes.Type(value = SearchOutputResponseDTO.class, name = "response")
}
)
public abstract class SearchOutputDTO {

}
