package es.us.lsi.fogallego.nlpindexer.patternextraction;

import es.us.lsi.fogallego.nlpindexer.dto.search.SearchRuleDTO;
import es.us.lsi.fogallego.nlpindexer.model.LabelledWordParent;
import es.us.lsi.fogallego.nlpindexer.model.Message;
import es.us.lsi.fogallego.nlpindexer.model.dataset.MatchedRule;
import es.us.lsi.fogallego.nlpindexer.model.dataset.Matching;
import es.us.lsi.fogallego.nlpindexer.model.util.Pair;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class SearchService {

    public static final String REGEX_GROUPS_REMOVE = "\\[|\\][^|]+";
    public static final String EMPTY = "";
    public static final String REGEX_VERTICAL_SEPARATOR = "\\|";
    public final String DOLLAR = "$";
    public final String HORIZONTAL_SEPARATOR = "-";
    public final String VERTICAL_SEPARATOR = "|";
    public final String DOLLAR_VAR_REGEXP = "\\$[0-9]+";

    public final Pattern dollarVarPattern = Pattern.compile(DOLLAR_VAR_REGEXP);

    private Map<String, List<Integer>> mapRuleIndexes = new HashMap<>();

    /**
     *
     * @param message
     * @param rule
     * @param ignoreInvalid
     * @return the list of filled templates
     */
    public List<Matching> getMatchingOfRule(Message message, SearchRuleDTO rule, Boolean ignoreInvalid) {

        List<LabelledWordParent> labelledWords = message.getLabelledWords();
        String messageTags;
        List<Integer> lstIndexesOfPattern = getLstIndexesOfPattern(rule);
        List<List<Pair<Integer, Integer>>> lstStartIndexes;


        if (ignoreInvalid) {
            messageTags = message.getNlpTag().getPatternWithInvalid();
        } else {
            messageTags = message.getNlpTag().getPattern();
        }

        lstStartIndexes = getLstStartIndexesOfPattern(rule.getSyntacticPattern(), messageTags);

        List<String> labelledWordsTokens = labelledWords.stream().map(lw -> lw.getToken()).collect(Collectors.toList());

        List<Matching> collect = lstStartIndexes.stream().map(
                lstOfGroup -> {
                    String filledTemplate = rule.getTemplate();
                    List<Pair<Integer, Integer>> lstTokenRange = new ArrayList<>();
                    for (Integer indexOfPattern : lstIndexesOfPattern) {
                        String replacement;
                        if (indexOfPattern == 0) {
                            replacement = message.getText();
                        } else {
                            Pair<Integer, Integer> group = lstOfGroup.get(indexOfPattern - 1);
                            lstTokenRange.add(group);
                            replacement = String.join(" ", labelledWordsTokens.subList(group.getFirst(), group.getSecond() + 1));
                        }

                        filledTemplate = filledTemplate.replace(DOLLAR + indexOfPattern, replacement);
                    }

                    return new Matching(filledTemplate, lstTokenRange);
                }
        ).collect(Collectors.toList());
        return collect;
    }

    /**
     *
     * @param rule
     * @return the list of indexes of pattern
     */
    private synchronized List<Integer> getLstIndexesOfPattern(SearchRuleDTO rule) {
        List<Integer> lstIndexesOfPattern;
        if (mapRuleIndexes.containsKey(rule.toString())) {
            lstIndexesOfPattern = mapRuleIndexes.get(rule.toString());
        } else {
            Matcher matcher = dollarVarPattern.matcher(rule.getTemplate());
            lstIndexesOfPattern = new ArrayList<>();
            while (matcher.find()) {
                String group = matcher.group();
                String replaced = group.replace(DOLLAR, "");
                Integer index = Integer.valueOf(replaced);
                lstIndexesOfPattern.add(index);
            }

            mapRuleIndexes.put(rule.toString(), lstIndexesOfPattern);
        }
        return lstIndexesOfPattern;
    }

    private List<List<Pair<Integer,Integer>>> getLstStartIndexesOfPattern(String syntacticPattern, String messageTags) {

        List<List<Pair<Integer,Integer>>> lstStartIndexes = new ArrayList<>();

        String canonizedMessageTags = messageTags.replaceAll(HORIZONTAL_SEPARATOR,VERTICAL_SEPARATOR);

        Matcher m = Pattern.compile(syntacticPattern).matcher(messageTags);

        while (m.find()) {
            int numGroups = m.groupCount();

            List<Pair<Integer,Integer>> lstStartIndexesOfGroup = new ArrayList<>();

            for (int i=1;i<=numGroups;i++) {
//                System.out.println(m.start(i) + " -> " + m.end(i) + ": " + m.group(i));
                String substring = canonizedMessageTags.substring(0, m.start(i));
                String canonizedSubstring = substring.replaceAll(REGEX_GROUPS_REMOVE, EMPTY);
                int start;
                int end;
                int lengthSplitGroup = m.group(i).split("[|-]").length;
                if (canonizedSubstring == null || canonizedSubstring.isEmpty()) {
                    start = 0;
                    end = lengthSplitGroup - 1;
                } else {
                    String[] split = canonizedSubstring.split(REGEX_VERTICAL_SEPARATOR);
                    start = split.length;
                    end = start + lengthSplitGroup - 1;
                }
                lstStartIndexesOfGroup.add(new Pair<>(start,end));
            }

            lstStartIndexes.add(lstStartIndexesOfGroup);
        }

        return lstStartIndexes;
    }

    public void detectEmbedded(List<MatchedRule> lstMatchedRule) {

        List<Matching> lstMatching = lstMatchedRule.stream().map(mr -> mr.getLstMatching())
                .flatMap(m -> m.stream())
                .collect(Collectors.toList());

        lstMatching.forEach(m -> m.setEmbedded(findIfIsEmbedded(m, lstMatching)));

    }

    private boolean findIfIsEmbedded(Matching matching, List<Matching> lstMatching) {
        boolean embedded = false;
        List<Pair<Integer, Integer>> lstTokenRange = matching.getLstTokenRange();

        for(int i=0;!embedded && i<lstMatching.size();i++) {
            Matching other = lstMatching.get(i);
            if (!matching.equals(other) && isEmbedded(matching, other)) {
                embedded = true;
                break;
            }
        }

        return embedded;
    }

    private boolean isEmbedded(Matching matching, Matching other) {

        List<Pair<Integer, Integer>> lstTokenRange = matching.getLstTokenRange();
        List<Pair<Integer, Integer>> lstTokenRangeOther = other.getLstTokenRange();

        int sizeLstTokenRange = lstTokenRange.size();
        if (sizeLstTokenRange <= lstTokenRangeOther.size()) {
            boolean embedded = false;
            for (Pair<Integer, Integer> integerPair : lstTokenRange) {
                boolean found = false;
                for (Pair<Integer, Integer> integerPairOther : lstTokenRangeOther) {
                    if (integerPair.getFirst() >= integerPairOther.getFirst()
                            && integerPair.getSecond() <= integerPairOther.getSecond()) {
                        found = true;
                        break;
                    }
                }
                embedded = found;
                if (!found) {
                    break;
                }
            }
            return embedded;
        } else {
            return false;
        }
    }
}
