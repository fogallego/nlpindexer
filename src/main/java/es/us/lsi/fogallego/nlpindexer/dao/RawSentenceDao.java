package es.us.lsi.fogallego.nlpindexer.dao;

import es.us.lsi.fogallego.nlpindexer.model.RawSentence;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface RawSentenceDao extends MongoRepository<RawSentence, String> {

    public RawSentence findByCorrectedDigest(String correctedDigest);
}
