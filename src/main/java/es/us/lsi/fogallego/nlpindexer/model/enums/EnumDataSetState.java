package es.us.lsi.fogallego.nlpindexer.model.enums;

public enum EnumDataSetState {

    CREATING,
    READY,
    DELETING

}
