package es.us.lsi.fogallego.nlpindexer.dto.search;

import com.wordnik.swagger.annotations.ApiModel;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@ApiModel
public class SearchParamsDTO {

    @NotNull
    private Boolean ignoreInvalid;
    @NotEmpty(message = "List of rules must not be empty")
    private List<SearchRuleDTO> rules;
    @Valid
    @NotNull
    private SearchOutputDTO output;

    public Boolean getIgnoreInvalid() {
        return ignoreInvalid;
    }

    public void setIgnoreInvalid(Boolean ignoreInvalid) {
        this.ignoreInvalid = ignoreInvalid;
    }

    public List<SearchRuleDTO> getRules() {
        return rules;
    }

    public void setRules(List<SearchRuleDTO> rules) {
        this.rules = rules;
    }

    public SearchOutputDTO getOutput() {
        return output;
    }

    public void setOutput(SearchOutputDTO output) {
        this.output = output;
    }
}
