package es.us.lsi.fogallego.nlpindexer.model;

public class WordToken implements java.io.Serializable{

    private String original;
    private String lemma;
    private String tag;

    public WordToken(String original, String lemma, String tag) {
        this.original=original;
        this.lemma=lemma;
        this.tag=tag;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String toString() {
        return "{ " + original + " " + lemma + " " + tag + " }";
    }
}
