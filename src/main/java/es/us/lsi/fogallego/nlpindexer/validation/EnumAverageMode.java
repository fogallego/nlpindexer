package es.us.lsi.fogallego.nlpindexer.validation;

/**
 * Created by forte on 20/01/2016.
 */
public enum EnumAverageMode {

    MICROAVERAGE, MACROAVERAGE

}
