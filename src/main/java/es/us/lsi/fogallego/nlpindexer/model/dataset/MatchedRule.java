package es.us.lsi.fogallego.nlpindexer.model.dataset;

import es.us.lsi.fogallego.nlpindexer.dto.search.SearchRuleDTO;

import java.util.List;

public class MatchedRule {

    private SearchRuleDTO searchRuleDTO;
    private List<Matching> lstMatching;

    public MatchedRule(SearchRuleDTO searchRuleDTO, List<Matching> lstMatching) {
        this.searchRuleDTO = searchRuleDTO;
        this.lstMatching = lstMatching;
    }

    public SearchRuleDTO getSearchRuleDTO() {
        return searchRuleDTO;
    }

    public void setSearchRuleDTO(SearchRuleDTO searchRuleDTO) {
        this.searchRuleDTO = searchRuleDTO;
    }

    public List<Matching> getLstMatching() {
        return lstMatching;
    }

    public void setLstMatching(List<Matching> lstMatching) {
        this.lstMatching = lstMatching;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MatchedRule)) return false;

        MatchedRule that = (MatchedRule) o;

        if (searchRuleDTO != null ? !searchRuleDTO.equals(that.searchRuleDTO) : that.searchRuleDTO != null)
            return false;
        return lstMatching != null ? lstMatching.equals(that.lstMatching) : that.lstMatching == null;

    }

    @Override
    public int hashCode() {
        int result = searchRuleDTO != null ? searchRuleDTO.hashCode() : 0;
        result = 31 * result + (lstMatching != null ? lstMatching.hashCode() : 0);
        return result;
    }
}
