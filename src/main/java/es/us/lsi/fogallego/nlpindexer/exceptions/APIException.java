package es.us.lsi.fogallego.nlpindexer.exceptions;

public class ApiException extends Exception {

    /** The Constant serialVersionUID. */
    private static final long	serialVersionUID	= 1L;

    /** The response status. */
    private int					responseStatusCode;

    /**
     * Instantiates a new NLPIndexer api exception.
     */
    public ApiException() {
        super();
    }

    /**
     * Instantiates a new NLPIndexer api exception.
     *
     * @param message the message
     * @param responseStatusCode the response status ode
     */
    public ApiException(String message, int responseStatusCode) {
        super(message);
        this.responseStatusCode = responseStatusCode;
    }

    public ApiException(Exception e) {
        super(e);
    }

    /**
     * @return the responseStatusCode
     */
    public int getResponseStatusCode() {
        return responseStatusCode;
    }

    /**
     * @param responseStatusCode the responseStatusCode to set
     */
    public void setResponseStatusCode(int responseStatusCode) {
        this.responseStatusCode = responseStatusCode;
    }

}
