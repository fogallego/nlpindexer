package es.us.lsi.fogallego.nlpindexer.controllers;

import com.wordnik.swagger.annotations.*;
import es.us.lsi.fogallego.nlpindexer.dao.*;
import es.us.lsi.fogallego.nlpindexer.dto.search.SearchOutputDTO;
import es.us.lsi.fogallego.nlpindexer.dto.search.SearchParamsDTO;
import es.us.lsi.fogallego.nlpindexer.dto.search.SearchRuleDTO;
import es.us.lsi.fogallego.nlpindexer.dto.search.output.SearchOutputDatasetDTO;
import es.us.lsi.fogallego.nlpindexer.dto.search.output.SearchOutputResponseDTO;
import es.us.lsi.fogallego.nlpindexer.exceptions.NLPIndexerApiException;
import es.us.lsi.fogallego.nlpindexer.model.dataset.*;
import es.us.lsi.fogallego.nlpindexer.model.enums.EnumDataSetState;
import es.us.lsi.fogallego.nlpindexer.patternextraction.SearchService;
import org.apache.catalina.connector.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Api("dataset")
@RestController
@RequestMapping(PathVariables.API + "dataset")
public class DataSetController {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    public static final String CSV_SEPARATOR = ";";
    @Autowired
    private MessageDaoCustom messageDaoCustom;

    @Autowired
    private DataSetDao dataSetDao;

    @Autowired
    private DataSetDaoCustom dataSetDaoCustom;

    @Autowired
    private DataSetElementFullDao dataSetElementFullDao;

    @Autowired
    private DataSetElementLiteDao dataSetElementLiteDao;

    @Autowired
    private SearchService searchService;

    @Autowired
    private RawSentenceDao rawSentenceDao;

    @ApiOperation(value = "Get DataSet info")
    @RequestMapping(method = RequestMethod.GET, value = "/{idDataSet}")
    public
    @ResponseBody
    DataSet getDataSet(@PathVariable String idDataSet) throws NLPIndexerApiException {

        DataSet dataSet = dataSetDao.findById(idDataSet);

        if (dataSet == null) {
            throw new NLPIndexerApiException("DataSet with id " + idDataSet + " doesn´t exists.", HttpStatus.NOT_FOUND.value());
        }

        return dataSet;
    }

    @ApiOperation(value = "Get DataSet info")
    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    List<DataSet> getDataSetList() {
        return dataSetDao.findAll();
    }

    @ApiOperation(value = "CreateDataSet")
    @ApiResponses({@ApiResponse(code = Response.SC_NOT_FOUND, message = "Message not found")})
    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    DataSet createDataSet(@ApiParam(value = "searchParamsDTO", required = true)
                          @RequestBody @Valid SearchParamsDTO searchParamsDTO) throws NLPIndexerApiException {

        DataSet dataSet = new DataSet();
        dataSet.setDatasetConfiguration(searchParamsDTO);
        dataSet.setState(EnumDataSetState.CREATING);
        SearchOutputDTO output = searchParamsDTO.getOutput();
        if (output instanceof SearchOutputDatasetDTO &&
                dataSetDaoCustom.existsDataSetWithName(((SearchOutputDatasetDTO) output).getName())) {
            NLPIndexerApiException nlpIndexerApiException = new NLPIndexerApiException("Dataset with the same name already exists",
                    HttpStatus.BAD_REQUEST.value());
            throw nlpIndexerApiException;
        }

        final DataSet storedDataSet = dataSetDao.insert(dataSet);

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            long tInit = System.currentTimeMillis();
            log.warn("Executing in new thread...");
            List<SearchRuleDTO> rules = searchParamsDTO.getRules();
            List<String> lstPatterns = rules
                    .stream().map(rule -> rule.getSyntacticPattern()).collect(Collectors.toList());
            messageDaoCustom.getMatchedMessages(lstPatterns, searchParamsDTO.getIgnoreInvalid())
                    .parallelStream()
                    .forEach(message -> {
                        DataSetElementFull dataSetElementFull = new DataSetElementFull();
                        dataSetElementFull.setOriginalText(rawSentenceDao
                                .findOne(message.getIdRawSentence()).getText());
                        dataSetElementFull.setMessage(message);
                        dataSetElementFull.setIdDataSet(dataSet.getId());
                        List<MatchedRule> lstMatchedRule = new ArrayList<>();
                        rules.forEach(rule -> {
                            List<Matching> lstMatching = searchService
                                    .getMatchingOfRule(message, rule, searchParamsDTO.getIgnoreInvalid());
                            if (!lstMatching.isEmpty()) {
                                lstMatchedRule.add(new MatchedRule(rule, lstMatching));
                            }
                        });
                        searchService.detectEmbedded(lstMatchedRule);
                        dataSetElementFull.setLstMatchedRule(lstMatchedRule);

                        dataSetElementFullDao.insert(dataSetElementFull);
                        dataSetElementLiteDao.insert(new DataSetElementLite(dataSetElementFull));
                    });

            tInit = System.currentTimeMillis() - tInit;
            log.warn("Finishing dataset creation!");

            storedDataSet.setNumberOfDataSetElements(dataSetElementFullDao.countByIdDataSet(storedDataSet.getId()));
            storedDataSet.setState(EnumDataSetState.READY);
            storedDataSet.setTimeToComplete((float) tInit);

            dataSetDao.save(storedDataSet);
        });

        return storedDataSet;
    }

    @ApiOperation(value = "Delete DataSet")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{idDataSet}")
    public
    @ResponseBody
    String deleteDataSet(@PathVariable String idDataSet) throws NLPIndexerApiException {

        Long numDeletedFull;
        Long numDeletedLite;
        Long time = System.currentTimeMillis();
        DataSet dataSet = dataSetDao.findById(idDataSet);
        if (dataSet == null) {
            throw new NLPIndexerApiException("DataSet with id " + idDataSet + " doesn´t exists.", HttpStatus.NOT_FOUND.value());
        }
        numDeletedFull = dataSetElementFullDao.deleteDataSetElementByIdDataSet(idDataSet);
        numDeletedLite = dataSetElementLiteDao.deleteDataSetElementByIdDataSet(idDataSet);
        dataSetDao.delete(dataSet);
        time = System.currentTimeMillis() - time;

        return "{\"numDataSetElementFullDeleted\" : \"" + numDeletedFull + "\"," +
                "\"numDataSetElementLiteDeleted \" : \"" + numDeletedLite + "\","
                + "  \"executionTime\"" + time + "}";
    }

    @RequestMapping(value = "/{idDataSet}/datasetelementsfull", method = RequestMethod.GET)
    public
    @ResponseBody
    List<DataSetElementFull> getDataSetElementsFull(@PathVariable String idDataSet,
                                            @RequestParam(required = false, defaultValue = "0") int page,
                                            @RequestParam(required = false, defaultValue = "10") int rpp)
            throws NLPIndexerApiException {

        Pageable pageable = getPageable(idDataSet, page, rpp);

        return dataSetElementFullDao.findByIdDataSet(idDataSet, pageable);
    }

    @RequestMapping(value = "/{idDataSet}/datasetelementslite", method = RequestMethod.GET)
    public
    @ResponseBody
    List<DataSetElementLite> getDataSetElementsLite(@PathVariable String idDataSet,
                                                @RequestParam(required = false, defaultValue = "0") int page,
                                                @RequestParam(required = false, defaultValue = "10") int rpp)
            throws NLPIndexerApiException {

        Pageable pageable = getPageable(idDataSet, page, rpp);

        return dataSetElementLiteDao.findByIdDataSet(idDataSet, pageable);
    }

    @RequestMapping(value = "/samplematching", method = RequestMethod.POST)
    public
    @ResponseBody
    List<DataSetElementLite> getSampleMatchingLite(@ApiParam(value = "searchParamsDTO", required = true)
                                               @RequestBody @Valid SearchParamsDTO searchParamsDTO) throws NLPIndexerApiException {

        SearchOutputDTO output = searchParamsDTO.getOutput();
        if (!(output instanceof SearchOutputResponseDTO)) {
            throw new NLPIndexerApiException("Wrong search params for sample matching", HttpStatus.BAD_REQUEST.value());
        }

        List<SearchRuleDTO> rules = searchParamsDTO.getRules();
        List<String> lstPatterns = rules
                .stream().map(rule -> rule.getSyntacticPattern()).collect(Collectors.toList());
        return messageDaoCustom.getSampleMatchedMessages(lstPatterns, searchParamsDTO.getIgnoreInvalid(),
                ((SearchOutputResponseDTO) output).getNumElements())
                .stream()
                .map(message -> {
                    DataSetElementFull dataSetElementFull = new DataSetElementFull();
                    dataSetElementFull.setOriginalText(rawSentenceDao
                            .findOne(message.getIdRawSentence()).getText());
                    dataSetElementFull.setMessage(message);
                    List<MatchedRule> lstMatchedRule = new ArrayList<>();
                    rules.forEach(rule -> {
                        List<Matching> lstMatching = searchService
                                .getMatchingOfRule(message, rule, searchParamsDTO.getIgnoreInvalid());
                        if (!lstMatching.isEmpty()) {
                            lstMatchedRule.add(new MatchedRule(rule, lstMatching));
                        }
                    });
                    searchService.detectEmbedded(lstMatchedRule);
                    dataSetElementFull.setLstMatchedRule(lstMatchedRule);

                    return new DataSetElementLite(dataSetElementFull);
                }).collect(Collectors.toList());

    }

    private Pageable getPageable(String idDataSet, int page, int rpp) throws NLPIndexerApiException {
        Pageable pageable = new PageRequest(page, rpp);

        DataSet dataSet = dataSetDao.findById(idDataSet);

        if (dataSet == null) {
            throw new NLPIndexerApiException("DataSet with id " + idDataSet + " doesn´t exists.", HttpStatus.NOT_FOUND.value());
        }

        switch (dataSet.getState()) {
            case CREATING:
                throw new NLPIndexerApiException("DataSet is not ready yet.", HttpStatus.LOCKED.value());
            case DELETING:
                throw new NLPIndexerApiException("DataSet is being deleted.", HttpStatus.GONE.value());
        }
        return pageable;
    }
}
