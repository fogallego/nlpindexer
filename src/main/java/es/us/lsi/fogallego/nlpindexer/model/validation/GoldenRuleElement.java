package es.us.lsi.fogallego.nlpindexer.model.validation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.DigestUtils;

import java.util.List;

@Document(collection = "goldenruleelement")
public class GoldenRuleElement {

    @Id
    private String id;
    @JsonIgnore
    @Transient
    private String idDataSet;
    private String text;
    @Indexed
    private String textDigest;
    @JsonIgnore
    @Transient
    private String corrected;
    @JsonIgnore
    @Transient
    private String idMessage;
    @JsonIgnore
    @Transient
    private String idRawSentence;
    private List<String> lstFilledTemplate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdDataSet() {
        return idDataSet;
    }

    public void setIdDataSet(String idDataSet) {
        this.idDataSet = idDataSet;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        this.textDigest = DigestUtils.md5DigestAsHex(text.getBytes());
    }

    public String getTextDigest() {
        return textDigest;
    }

    public String getCorrected() {
        return corrected;
    }

    public void setCorrected(String corrected) {
        this.corrected = corrected;
    }

    public String getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(String idMessage) {
        this.idMessage = idMessage;
    }

    public void setIdRawSentence(String idRawSentence) {
        this.idRawSentence = idRawSentence;
    }

    public List<String> getLstFilledTemplate() {
        return lstFilledTemplate;
    }

    public void setLstFilledTemplate(List<String> lstFilledTemplate) {
        this.lstFilledTemplate = lstFilledTemplate;
    }
}
