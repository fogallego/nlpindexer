package es.us.lsi.fogallego.nlpindexer.dao;

import es.us.lsi.fogallego.nlpindexer.model.validation.GoldenRuleElement;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface GoldenRuleElementDao extends MongoRepository<GoldenRuleElement,String> {

//    public GoldenRuleElement findByIdRawSentence(String idRawSentence);

    public GoldenRuleElement findByTextDigest(String textDigest);
}
