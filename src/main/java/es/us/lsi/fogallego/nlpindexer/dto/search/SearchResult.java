package es.us.lsi.fogallego.nlpindexer.dto.search;

import com.wordnik.swagger.annotations.ApiModel;

@ApiModel
public class SearchResult {

    private Boolean correct;
    private Float executionTime;
    private String jsonResult;

    public SearchResult(boolean correct, Float executionTime) {
        this.correct = correct;
        this.executionTime = executionTime;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }

    public Float getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Float executionTime) {
        this.executionTime = executionTime;
    }

    public String getJsonResult() {
        return jsonResult;
    }

    public void setJsonResult(String jsonResult) {
        this.jsonResult = jsonResult;
    }
}
