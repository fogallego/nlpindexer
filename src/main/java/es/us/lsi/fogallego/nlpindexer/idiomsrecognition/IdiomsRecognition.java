package es.us.lsi.fogallego.nlpindexer.idiomsrecognition;

import au.com.bytecode.opencsv.CSVReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class IdiomsRecognition {

    public static final String SPACE = " ";
    @Value("${dicts.idioms}")
    private String idiomsFile;

    private Map<String, List<Idiom>> mapIdiom;

    private synchronized void loadIdioms() throws FileNotFoundException, UnsupportedEncodingException, IOException {

        if (mapIdiom == null) {
            mapIdiom = new HashMap<>();
            CSVReader reader = new CSVReader(new InputStreamReader(new FileInputStream(idiomsFile), "UTF-8"),';');
            reader.readAll().stream()
                    .forEach(line -> {
                        String idiomStr = line[0];
                        Idiom idiom = new Idiom(idiomStr,Integer.valueOf(line[1]));
                        String key = idiomStr.split(SPACE,2)[0];

                        List<Idiom> lstIdioms = mapIdiom.get(key);
                        if (lstIdioms == null) {
                            lstIdioms = new ArrayList<>();
                            mapIdiom.put(key,lstIdioms);
                        }

                        lstIdioms.add(idiom);
                    });
        }
    }

    public List<Integer> getPositionsOfIdioms(List<String> lstTokens) throws IOException {

        loadIdioms();

        List<Integer> lstPosition = new ArrayList<>();

        for (int i = 0; i < lstTokens.size(); i++) {

            List<Idiom> candidates = mapIdiom.get(lstTokens.get(i));
            Idiom lastMatch = null;
            int pointer = 1;
            int intermediate = 0;
            int lastPosition = i;

            for (int j = i + 1; candidates != null && !candidates.isEmpty() && j < lstTokens.size(); j++) {
                String token = lstTokens.get(j);
                List<Idiom> matches = new ArrayList<Idiom>();
                // Filtramos los candidatos y obtenemos los que encajan por completo y los que temporalmente encajan
                // (matches y candidates respectivamente)
                // Nos quedamos con el último que haya encajado totalmente
                Idiom match = checkCandidates(pointer, intermediate, candidates, token, matches);
                if (match != null) {
                    // Si alguno encaja totalmente se anota tanto él como la posición de su último unigram
                    lastPosition = j;
                    lastMatch = match;
                }
                // Si no hay ningún candidato que encaje totalmente
                if (matches.isEmpty()) {
                    intermediate++;
                } else {
                    candidates.clear();
                    candidates.addAll(matches);
                    matches.clear();
                    pointer++;
                }
            }

            if (lastMatch != null) {
                for (;i<=lastPosition;i++) {
                    lstPosition.add(i);
                }
                i = lastPosition;
            }

        }


        return lstPosition;
    }

    /**
     * Dado un token y una posición se comprueba qué idioms encajan temporalmente en dicha posición.
     *
     * @param pointer
     *            the pointer
     * @param intermediate
     *            the intermediate
     * @param candidates
     *            the candidates
     * @param token
     *            the token
     * @param matches
     *            the matches
     * @return the polarity idiom
     */
    private Idiom checkCandidates(int pointer, int intermediate, List<Idiom> candidates,
                                                 String token, List<Idiom> matches) {
        Idiom match = null;
        for (int j = 0; j < candidates.size(); j++) {
            Idiom candidate = candidates.get(j);
            // Check if current candidate does not satisfy the requirements
            List<String> lstToken = candidate.getLstToken();
            if (pointer < lstToken.size() && intermediate <= candidate.getMaxIntermediate()
                    && lstToken.get(pointer).equals(token)) {
                // If candidate is the idiom searched
                if (pointer == lstToken.size() - 1) {
                    match = candidate;
                    // If candidate match this stem but it has more stems
                } else {
                    matches.add(candidate);
                }
            }

        }
        return match;
    }

}
