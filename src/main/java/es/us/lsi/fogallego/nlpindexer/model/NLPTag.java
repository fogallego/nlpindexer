package es.us.lsi.fogallego.nlpindexer.model;

public class NLPTag {

    private String eagle;
    private String own;
    private String ownGrouped;
    private String eagleIdiom;
    private String ownIdiom;
    private String ownGroupedIdiom;
    private String pattern;
    private String patternWithInvalid;

    public String getEagle() {
        return eagle;
    }

    public void setEagle(String eagle) {
        this.eagle = eagle;
    }

    public String getOwn() {
        return own;
    }

    public void setOwn(String own) {
        this.own = own;
    }

    public String getOwnGrouped() {
        return ownGrouped;
    }

    public void setOwnGrouped(String ownGrouped) {
        this.ownGrouped = ownGrouped;
    }

    public String getEagleIdiom() {
        return eagleIdiom;
    }

    public void setEagleIdiom(String eagleIdiom) {
        this.eagleIdiom = eagleIdiom;
    }

    public String getOwnIdiom() {
        return ownIdiom;
    }

    public void setOwnIdiom(String ownIdiom) {
        this.ownIdiom = ownIdiom;
    }

    public String getOwnGroupedIdiom() {
        return ownGroupedIdiom;
    }

    public void setOwnGroupedIdiom(String ownGroupedIdiom) {
        this.ownGroupedIdiom = ownGroupedIdiom;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getPatternWithInvalid() {
        return patternWithInvalid;
    }

    public void setPatternWithInvalid(String patternWithInvalid) {
        this.patternWithInvalid = patternWithInvalid;
    }
}
