package es.us.lsi.fogallego.nlpindexer.dto;

import com.wordnik.swagger.annotations.ApiModel;

import java.util.List;

@ApiModel
public class LabelledWordDTO {

    private Integer position;
    private String ownTag;
    private String eagle;
    private List<LabelledWordDTO> subLabelledWordList;
    private String token;
    private String lemma;

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getOwnTag() {
        return ownTag;
    }

    public void setOwnTag(String ownTag) {
        this.ownTag = ownTag;
    }

    public String getEagle() {
        return eagle;
    }

    public void setEagle(String eagle) {
        this.eagle = eagle;
    }

    public List<LabelledWordDTO> getSubLabelledWordList() {
        return subLabelledWordList;
    }

    public void setSubLabelledWordList(List<LabelledWordDTO> subLabelledWordList) {
        this.subLabelledWordList = subLabelledWordList;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }
}
