package es.us.lsi.fogallego.nlpindexer.dao;

import es.us.lsi.fogallego.nlpindexer.model.Message;
import es.us.lsi.fogallego.nlpindexer.model.PosTaggedSentence;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public interface MessageDaoCustom {

    Message indexMessageFromSentence(PosTaggedSentence sentence) ;

    Message indexMessageFromSentence(PosTaggedSentence sentence, String idRawSentence) ;

    public void extractAndSaveFromFile(String fileIn) throws IOException;

    public List<Message> getMatchedMessages (List<String> lstPatterns, Boolean ignoreInvalid);

    public List<Message> getSampleMatchedMessages (List<String> lstPatterns, Boolean ignoreInvalid, int sizeSample);

    Message convert2Message(PosTaggedSentence posTaggedSentence);
}
