package es.us.lsi.fogallego.nlpindexer.dao;

import es.us.lsi.fogallego.nlpindexer.model.RawText;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface RawTextDao extends MongoRepository<RawText, String> {

    public RawText findByTextDigest(String textDigest);

}
