package es.us.lsi.fogallego.nlpindexer.exceptions;

import org.springframework.http.HttpStatus;

public class ValidationException extends GoldenRuleException {
    public ValidationException(String text) {
        super(text, HttpStatus.INTERNAL_SERVER_ERROR.value());
    }
}
