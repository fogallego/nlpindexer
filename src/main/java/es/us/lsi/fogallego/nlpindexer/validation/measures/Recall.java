package es.us.lsi.fogallego.nlpindexer.validation.measures;

import es.us.lsi.fogallego.nlpindexer.model.validation.ConfusionMatrix;
import es.us.lsi.fogallego.nlpindexer.validation.AbstractValidationMeasure;

public class Recall extends AbstractValidationMeasure {

    public static final String RECALL = "Recall";

    @Override
    protected String measureName() {
        return RECALL;
    }

    @Override
    protected Double calculateWithMicroAverage(ConfusionMatrix confusionMatrix) {
        double value;

        if (confusionMatrix.getTruePositive() == 0 && confusionMatrix.getFalseNegative() == 0) {
            value = 1.0;
        } else {
            value = ((double) confusionMatrix.getTruePositive()) /
                    (confusionMatrix.getTruePositive() + confusionMatrix.getFalseNegative());
        }
        return value;
    }
}
