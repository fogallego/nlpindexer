package es.us.lsi.fogallego.nlpindexer.controllers;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import org.apache.catalina.connector.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

/**
 * The Class TestController.
 */
@Api("test")
@RestController
@RequestMapping(value = "test")
public class TestController {

	/** The log. */
	protected Logger	log	= LoggerFactory.getLogger(this.getClass());

	/**
	 * Dummy test.
	 */
	@ApiOperation(value = "Test endpoint")
	@ApiResponses({@ApiResponse(code = Response.SC_OK, message = "Test works correctly")})
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody Object test(@RequestParam String paramStr) {

		log.warn("Hello world!!");

		return "ok";
	}
}
