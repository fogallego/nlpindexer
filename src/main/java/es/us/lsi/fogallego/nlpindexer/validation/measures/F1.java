package es.us.lsi.fogallego.nlpindexer.validation.measures;

import es.us.lsi.fogallego.nlpindexer.model.validation.ConfusionMatrix;
import es.us.lsi.fogallego.nlpindexer.validation.AbstractValidationMeasure;

public class F1 extends AbstractValidationMeasure {

    public static final String F1 = "F1";

    @Override
    protected String measureName() {
        return F1;
    }

    @Override
    protected Double calculateWithMicroAverage(ConfusionMatrix confusionMatrix) {

        return ((double) 2 * confusionMatrix.getTruePositive()) /
                ( 2 * confusionMatrix.getTruePositive()
                        + confusionMatrix.getFalsePositive()
                        + confusionMatrix.getFalseNegative());
    }
}
