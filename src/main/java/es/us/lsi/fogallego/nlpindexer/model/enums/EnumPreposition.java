package es.us.lsi.fogallego.nlpindexer.model.enums;

public enum EnumPreposition {

    OF,WITH,AT,IN,FROM,UNDER,FOR,UNTIL,OTHER

}
