package es.us.lsi.fogallego.nlpindexer.validation.measures;

import es.us.lsi.fogallego.nlpindexer.model.validation.ConfusionMatrix;
import es.us.lsi.fogallego.nlpindexer.validation.AbstractValidationMeasure;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Accuracy extends AbstractValidationMeasure {
    private static final String ACC = "Accuracy";

    @Override
    protected String measureName() {
        return ACC;
    }

    @Override
    protected Double calculateWithMicroAverage(ConfusionMatrix confusionMatrix) {

        // (TP + TN) / (TP + TN + FP + FN)

        BigDecimal tpBigInt = new BigDecimal(String.valueOf(confusionMatrix.getTruePositive()));
        BigDecimal fpBigInt = new BigDecimal(String.valueOf(confusionMatrix.getFalsePositive()));
        BigDecimal fnBigInt = new BigDecimal(String.valueOf(confusionMatrix.getFalseNegative()));

        BigDecimal tSumBigInt = tpBigInt.add(new BigDecimal(confusionMatrix.getTrueNegative().toString()));
        BigDecimal divisor = tSumBigInt.add(fpBigInt).add(fnBigInt);

        return tSumBigInt.divide(divisor, RoundingMode.HALF_UP).doubleValue();
    }
}
