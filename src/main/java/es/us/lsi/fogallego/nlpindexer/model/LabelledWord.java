package es.us.lsi.fogallego.nlpindexer.model;

import es.us.lsi.fogallego.nlpindexer.model.enums.EnumOwnTag;

public class LabelledWord {

    private Integer position;
    private EnumOwnTag ownTag;
    private String eagle;
    private String token;
    private String lemma;
    private boolean idiom;

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public EnumOwnTag getOwnTag() {
        return ownTag;
    }

    public void setOwnTag(EnumOwnTag ownTag) {
        this.ownTag = ownTag;
    }

    public String getEagle() {
        return eagle;
    }

    public void setEagle(String eagle) {
        this.eagle = eagle;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public boolean isIdiom() {
        return idiom;
    }

    public void setIdiom(boolean idiom) {
        this.idiom = idiom;
    }
}
