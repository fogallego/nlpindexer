package es.us.lsi.fogallego.nlpindexer.model;

public class BasicSemanticSkeleton {

    private String feature;
    private String value;

    public BasicSemanticSkeleton() {
    }

    public BasicSemanticSkeleton(String feature, String value) {
        this.feature = feature;
        this.value = value;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
