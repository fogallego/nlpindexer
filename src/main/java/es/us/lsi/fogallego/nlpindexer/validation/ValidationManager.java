package es.us.lsi.fogallego.nlpindexer.validation;

import es.us.lsi.fogallego.nlpindexer.dto.DataSetValidationDTO;
import es.us.lsi.fogallego.nlpindexer.exceptions.ValidationException;
import es.us.lsi.fogallego.nlpindexer.model.dataset.DataSetElementLite;
import es.us.lsi.fogallego.nlpindexer.model.validation.ConfusionMatrix;
import es.us.lsi.fogallego.nlpindexer.model.validation.GoldenRuleElement;
import org.apache.commons.math3.exception.MathArithmeticException;
import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.util.CombinatoricsUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.RegexPatternTypeFilter;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class ValidationManager {

    public static final String PACKAGE_MEASURES = "es.us.lsi.fogallego.nlpindexer.validation.measures";
    private List<AbstractValidationMeasure> lstValidationMeasure;
    private Map<Integer, BigInteger> cacheBigIntComb;

    public ValidationManager() {
        lstValidationMeasure = getListValidationMeasure();
        cacheBigIntComb = new HashMap<>();
    }

    public DataSetValidationDTO getDataSetValidationDTO(List<GoldenRuleElement> lstGoldenRuleElement,
                                                        List<DataSetElementLite> lstDataSetElementLite,
                                                        Long sizeDataSet) {

        DataSetValidationDTO dataSetValidationDTO = new DataSetValidationDTO();
        dataSetValidationDTO.setDataSetSize(sizeDataSet);
        dataSetValidationDTO.setGoldenRuleSize(lstGoldenRuleElement.size());
        dataSetValidationDTO.setNumDataSetElementsInGoldenRule(lstDataSetElementLite.size());

        List<ConfusionMatrix> lstConfusionMatrix = getListConfusionMatrix(lstGoldenRuleElement, lstDataSetElementLite);

        dataSetValidationDTO.setLstValidationMeasureMacroAverage(
                lstValidationMeasure.stream()
                        .map(vm -> vm.calculate(lstConfusionMatrix, EnumAverageMode.MACROAVERAGE))
                        .collect(Collectors.toList()));

        dataSetValidationDTO.setLstValidationMeasureMicroAverage(
                lstValidationMeasure.stream()
                        .map(vm -> vm.calculate(lstConfusionMatrix, EnumAverageMode.MICROAVERAGE))
                        .collect(Collectors.toList()));

        return dataSetValidationDTO;
    }

    private synchronized List<AbstractValidationMeasure> getListValidationMeasure() {

        if (lstValidationMeasure == null || lstValidationMeasure.isEmpty()) {
            lstValidationMeasure = new ArrayList<>();
            // create scanner and disable default filters (that is the 'false' argument)
            final ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
            // add include filters which matches all the classes (or use your own)
            provider.addIncludeFilter(new RegexPatternTypeFilter(Pattern.compile(".*")));

            // get matching classes defined in the package
            final Set<BeanDefinition> classes = provider.findCandidateComponents(PACKAGE_MEASURES);

            // this is how you can load the class type from BeanDefinition instance
            for (BeanDefinition bean : classes) {
                try {
                    Class<?> clazz = Class.forName(bean.getBeanClassName());

                    try {
                        AbstractValidationMeasure validationMeasure = (AbstractValidationMeasure) clazz.newInstance();
                        lstValidationMeasure.add(validationMeasure);
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }

                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }

        return lstValidationMeasure;
    }

    private List<ConfusionMatrix> getListConfusionMatrix(List<GoldenRuleElement> lstGoldenRuleElement,
                                                         List<DataSetElementLite> lstDataSetElementLite) {

        List<ConfusionMatrix> lstConfusionMatrix = new ArrayList<>();
        Map<String, List<DataSetElementLite>> mapFilledTemplate = new HashMap<>();
        lstDataSetElementLite.stream()
                .forEach(del -> {
                    List<DataSetElementLite> lstAux = mapFilledTemplate.get(del.getTextDigest());
                    if (lstAux == null) {
                        lstAux = new ArrayList<DataSetElementLite>();
                    }

                    lstAux.add(del);
                    mapFilledTemplate.put(del.getTextDigest(), lstAux);
                });

        lstGoldenRuleElement.forEach(
                gre -> {
                    List<DataSetElementLite> lstAuxDataSetElementsLite = mapFilledTemplate.get(gre.getTextDigest());
                    if (lstAuxDataSetElementsLite != null) {
                        lstAuxDataSetElementsLite
                                .forEach(dataSetElementLite -> {

                                            if (dataSetElementLite != null) {
                                                long truePositive = 0;
                                                long falsePositive;
                                                BigInteger trueNegative;
                                                long falseNegative = 0;
                                                List<String> lstFilledTemplateGoldenRule = gre.getLstFilledTemplate();
                                                Set<String> setFilledTemplateDataSet = dataSetElementLite.getLstFilledTemplate()
                                                        .stream()
                                                        .collect(Collectors.toCollection(HashSet::new));

                                                for (String filledTemplateGoldenRule : lstFilledTemplateGoldenRule) {
                                                    if (setFilledTemplateDataSet.contains(filledTemplateGoldenRule)) {
                                                        truePositive++;
                                                    } else {
                                                        falseNegative++;
                                                    }
                                                }

                                                falsePositive = setFilledTemplateDataSet.size() - truePositive;

                                                trueNegative = getTrueNegative(dataSetElementLite.getNumTokens(),
                                                        truePositive + falsePositive + falseNegative);

                                                ConfusionMatrix confusionMatrix =
                                                        new ConfusionMatrix(truePositive, falsePositive,
                                                        trueNegative, falseNegative);
                                                lstConfusionMatrix.add(confusionMatrix);
                                            }
                                        }
                                );
                    }
                }

        );

        return lstConfusionMatrix;
    }

    private BigInteger getTrueNegative(int numTokens, long nonTrueNegative) {

        if (numTokens == 0) {
            throw new RuntimeException(new ValidationException("Strange situation with a number of tokens equals to 0."));
        } else if (numTokens == 1) {
            return new BigInteger("0");
        } else if (numTokens == 2) {
            return new BigInteger("2");
        }

        BigInteger trueNegative = cachedComb(numTokens);

        BigInteger bigIntNonTrueNegative = new BigInteger(String.valueOf(nonTrueNegative));

        if (trueNegative.compareTo(bigIntNonTrueNegative) >= 0) {
            return trueNegative.subtract(bigIntNonTrueNegative);
        } else {
            throw new RuntimeException(new ValidationException("Strange situation with TrueNegative lesser than NonTrueNegative sum."));
        }
    }

    private BigInteger cachedComb(int numTokens) {

        BigInteger trueNegative;

        if (cacheBigIntComb.containsKey(numTokens)) {
            trueNegative = cacheBigIntComb.get(numTokens);
        } else {

            trueNegative = new BigInteger("0");

            for (int f = 1; f < numTokens; f++) {
                for (int v = 1; v < numTokens - f; v++) {

                    BigInteger featureComb;
                    try {
                        featureComb = new BigInteger(String.valueOf(CombinatoricsUtils.binomialCoefficient(numTokens, f)));
                    } catch (MathArithmeticException | NumberIsTooLargeException e) {
                        featureComb = bigIntComb(numTokens, f);
                    }

                    BigInteger valueComb;
                    int nComb = numTokens - f;
                    try {

                        valueComb = new BigInteger(String.valueOf(CombinatoricsUtils.binomialCoefficient(nComb, v)));
                    } catch (MathArithmeticException e) {
                        valueComb = bigIntComb(nComb, v);
                    }

                    trueNegative = trueNegative.add(featureComb.multiply(valueComb));
                }
            }

            cacheBigIntComb.put(numTokens, trueNegative);
        }

        return trueNegative;
    }

    private BigInteger bigIntComb(int n, int k) {
        BigInteger ret = BigInteger.ONE;
        for (int i = 0; i < k; i++) {
            ret = ret.multiply(BigInteger.valueOf(n-i))
                    .divide(BigInteger.valueOf(i+1));
        }
        return ret;
    }
}
