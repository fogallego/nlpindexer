package es.us.lsi.fogallego.nlpindexer.dto.search.output;

import es.us.lsi.fogallego.nlpindexer.dto.search.SearchOutputDTO;

public class SearchOutputFileDTO extends SearchOutputDTO {

    private String format;
    private String path;

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
