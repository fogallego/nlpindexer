package es.us.lsi.fogallego.nlpindexer.controllers;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import es.us.lsi.fogallego.nlpindexer.dao.DataSetElementLiteDao;
import es.us.lsi.fogallego.nlpindexer.dao.GoldenRuleElementDao;
import es.us.lsi.fogallego.nlpindexer.dto.DataSetValidationDTO;
import es.us.lsi.fogallego.nlpindexer.model.dataset.DataSetElementLite;
import es.us.lsi.fogallego.nlpindexer.model.validation.GoldenRuleElement;
import es.us.lsi.fogallego.nlpindexer.validation.ValidationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

//TODO
@Api("validation")
@RestController
@RequestMapping(PathVariables.API + "validation")
public class ValidationController {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private GoldenRuleElementDao goldenRuleElementDao;

    @Autowired
    private DataSetElementLiteDao dataSetElementLiteDao;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private ValidationManager validationManager;

    @ApiOperation(value = "Get List of GoldenRuleElements")
    @RequestMapping(method = RequestMethod.GET, value = "/goldenrule")
    public
    @ResponseBody List<GoldenRuleElement> getGoldenRuleElements(@RequestParam(required = false, defaultValue = "0") int page,
                                                                @RequestParam(required = false, defaultValue = "10") int rpp) {

        Pageable pageable = new PageRequest(page, rpp);

        return goldenRuleElementDao.findAll(pageable).getContent();
    }

    @ApiOperation(value = "Add or update GoldenRuleElements")
    @RequestMapping(method = RequestMethod.POST, value = "/goldenrule")
    public
    @ResponseBody int postGoldenRuleElements(@RequestBody @Valid List<GoldenRuleElement> lstGoldenRuleElement) {

        lstGoldenRuleElement.forEach(
                gre -> {
                    GoldenRuleElement greFromDB = goldenRuleElementDao.findByTextDigest(gre.getTextDigest());
                    if (greFromDB != null) {
                        greFromDB.setLstFilledTemplate(gre.getLstFilledTemplate());
                    } else {
                        greFromDB = gre;
                    }
                    goldenRuleElementDao.save(greFromDB);
                }
        );

        return lstGoldenRuleElement.size();
    }

//    @ApiOperation(value = "Add GoldenRuleElements or throw exception if exists")
//    @RequestMapping(method = RequestMethod.PUT, value = "/goldenrule")
//    public
//    @ResponseBody int putGoldenRuleElements(@RequestBody @Valid List<GoldenRuleElement> lstGoldenRuleElement) {
//        try {
//            goldenRuleElementDao.insert(lstGoldenRuleElement);
//        } catch (DuplicateKeyException exception) {
//            throw new GoldenRuleException("GoldenRuleElement with id "+, HttpStatus.BAD_REQUEST);
//        }
//        return lstGoldenRuleElement.size();
//    }

    @ApiOperation(value = "Get List of GoldenRuleElements")
    @RequestMapping(method = RequestMethod.DELETE, value = "/goldenrule")
    public
    @ResponseBody int deleteGoldenRuleElement() {

        long countBefore = goldenRuleElementDao.count();
        goldenRuleElementDao.deleteAll();
        long countAfter = goldenRuleElementDao.count();

        return (int) (countBefore - countAfter);
    }

    @ApiOperation(value = "Get List of GoldenRuleElements")
    @RequestMapping(method = RequestMethod.DELETE, value = "/goldenrule/{idGoldenRuleElement}")
    public
    @ResponseBody Boolean deleteGoldenRuleElementById(@PathVariable @Valid String idGoldenRuleElement) {

        goldenRuleElementDao.delete(idGoldenRuleElement);

        return true;
    }

    @ApiOperation(value = "Get List of GoldenRuleElements")
    @RequestMapping(method = RequestMethod.GET, value = "/{idDataSet}/validate")
    public
    @ResponseBody
    DataSetValidationDTO validateDataSet(@PathVariable @Valid String idDataSet) {

        List<GoldenRuleElement> lstGoldenRuleElement = goldenRuleElementDao.findAll();
        Long sizeDataSet = dataSetElementLiteDao.countByIdDataSet(idDataSet);
        List<String> lstTextDigest = lstGoldenRuleElement.stream().map(
                gre -> gre.getTextDigest()
        ).collect(Collectors.toList());

        Criteria criteriaDataSetsElements = Criteria.where("textDigest").in(lstTextDigest)
                .and("idDataSet").is(idDataSet);

        List<DataSetElementLite> lstDataSetElementLite = mongoTemplate.find(Query.query(criteriaDataSetsElements),
                DataSetElementLite.class);

        DataSetValidationDTO dataSetValidationDTO = validationManager.getDataSetValidationDTO(lstGoldenRuleElement,
                lstDataSetElementLite, sizeDataSet);
        dataSetValidationDTO.setIdDataSet(idDataSet);

        return dataSetValidationDTO;
    }

}
