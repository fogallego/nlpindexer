package es.us.lsi.fogallego.nlpindexer.validation;

import es.us.lsi.fogallego.nlpindexer.dto.ValidationMeasureDTO;
import es.us.lsi.fogallego.nlpindexer.model.validation.ConfusionMatrix;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.DoubleStream;

public abstract class AbstractValidationMeasure {

    /** The log. */
    protected Logger log	= LoggerFactory.getLogger(this.getClass());

    public ValidationMeasureDTO calculate(List<ConfusionMatrix> lstConfusionMatrix, EnumAverageMode enumAverageMode) {

        Double value;

        switch (enumAverageMode) {
            case MACROAVERAGE:
                value = calculateWithMacroAverage(lstConfusionMatrix);
                break;
            case MICROAVERAGE:
            default:
                ConfusionMatrix confusionMatrix = new ConfusionMatrix();
                lstConfusionMatrix.forEach(cm -> confusionMatrix.aggregate(cm));
                value = calculateWithMicroAverage(confusionMatrix);
        }

        return new ValidationMeasureDTO(measureName(), value);
    }


    protected abstract String measureName();

    /**
     * Micro-average approach
     *
     * @param confusionMatrix
     * @return the value of measure for param confusionMatrix
     */
    protected abstract Double calculateWithMicroAverage(ConfusionMatrix confusionMatrix);

    /**
     * Macro-average approach.
     *
     * @param lstConfusionMatrix
     * @return the average of measure for each measure value of each ConfusionMatrix
     */
    protected Double calculateWithMacroAverage(List<ConfusionMatrix> lstConfusionMatrix) {

//        log.warn("Confusion matrix: ");
//        log.warn("Size: " + lstConfusionMatrix.size());
//        log.warn("Lst: " + String.join(", ", lstConfusionMatrix.stream().map(ConfusionMatrix::toString)
//                .collect(Collectors.toList())));

        DoubleStream doubleStream = lstConfusionMatrix.stream().mapToDouble(cm -> this.calculateWithMicroAverage(cm));
        log.debug("DoubleStream: " + doubleStream);
        OptionalDouble average = doubleStream
                .average();
        log.debug("Average: " + average);

        return average
                .getAsDouble();

    }

}
