package es.us.lsi.fogallego.nlpindexer.dto;

import com.wordnik.swagger.annotations.ApiModel;

@ApiModel
public class NLPTagDTO {

    private String eagle;
    private String own;
    private String ownGrouped;

    public String getEagle() {
        return eagle;
    }

    public void setEagle(String eagle) {
        this.eagle = eagle;
    }

    public String getOwn() {
        return own;
    }

    public void setOwn(String own) {
        this.own = own;
    }

    public String getOwnGrouped() {
        return ownGrouped;
    }

    public void setOwnGrouped(String ownGrouped) {
        this.ownGrouped = ownGrouped;
    }

}
