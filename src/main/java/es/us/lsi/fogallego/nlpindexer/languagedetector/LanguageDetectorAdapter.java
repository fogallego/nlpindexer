package es.us.lsi.fogallego.nlpindexer.languagedetector;

import es.us.lsi.fogallego.languagedetector.languagedetection.LanguageDetectorManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LanguageDetectorAdapter {

    @Value("${dicts.langdetect}")
    private String langPath = "C:\\langprofiles";
    private LanguageDetectorManager lManager;

    public LanguageDetectorAdapter() {
        lManager = new LanguageDetectorManager(langPath);
    }

    public boolean isProbablySpanish (String text) {
        return lManager.isProbablySpanish(text);
    }

    public String detectLanguage (String text) {
        return lManager.detectLanguage(text);
    }
}
