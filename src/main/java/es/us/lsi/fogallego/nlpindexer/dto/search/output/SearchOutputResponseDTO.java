package es.us.lsi.fogallego.nlpindexer.dto.search.output;

import es.us.lsi.fogallego.nlpindexer.dto.search.SearchOutputDTO;

public class SearchOutputResponseDTO extends SearchOutputDTO {

    private int numElements;

    public int getNumElements() {
        return numElements;
    }

    public void setNumElements(int numElements) {
        this.numElements = numElements;
    }
}
