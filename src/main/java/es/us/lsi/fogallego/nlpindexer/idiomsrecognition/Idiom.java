package es.us.lsi.fogallego.nlpindexer.idiomsrecognition;

import java.util.Arrays;
import java.util.List;

public class Idiom {

    public static final String SPACE = " ";
    private int maxIntermediate;
    private List<String> lstToken;

    public Idiom(String idiom, int maxIntermediate) {
        lstToken = Arrays.asList(idiom.split(SPACE));
        this.maxIntermediate = maxIntermediate;
    }

    public int getMaxIntermediate() {
        return maxIntermediate;
    }

    public void setMaxIntermediate(int maxIntermediate) {
        this.maxIntermediate = maxIntermediate;
    }

    public List<String> getLstToken() {
        return lstToken;
    }

    public void setLstToken(List<String> lstToken) {
        this.lstToken = lstToken;
    }
}
