package es.us.lsi.fogallego.nlpindexer.dto.search;

public class SearchRuleDTO {
    private String syntacticPattern;
    private String template;

    public String getSyntacticPattern() {
        return syntacticPattern;
    }

    public void setSyntacticPattern(String syntacticPattern) {
        this.syntacticPattern = syntacticPattern;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    @Override
    public String toString() {
        return "SearchRuleDTO{" +
                "syntacticPattern='" + syntacticPattern + '\'' +
                ", template='" + template + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchRuleDTO that = (SearchRuleDTO) o;

        return !(syntacticPattern != null ? !syntacticPattern.equals(that.syntacticPattern) : that.syntacticPattern != null) && !(template != null ? !template.equals(that.template) : that.template != null);

    }

    @Override
    public int hashCode() {
        int result = syntacticPattern != null ? syntacticPattern.hashCode() : 0;
        result = 31 * result + (template != null ? template.hashCode() : 0);
        return result;
    }
}
