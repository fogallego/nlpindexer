package es.us.lsi.fogallego.nlpindexer.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.DigestUtils;

@Document(collection = "rawsentence")
public class RawSentence {
    @Id
    private String id;
    private String text;
    @Indexed
    private String textDigest;
    private String corrected;
    @Indexed
    private String correctedDigest;
    @Indexed
    private String idText;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        this.textDigest = DigestUtils.md5DigestAsHex(text.getBytes());
    }

    public String getCorrected() {
        return corrected;
    }

    public void setCorrected(String corrected) {
        this.corrected = corrected;
        this.correctedDigest = DigestUtils.md5DigestAsHex(corrected.getBytes());
    }

    public String getIdText() {
        return idText;
    }

    public void setIdText(String idText) {
        this.idText = idText;
    }

    public String getTextDigest() {
        return textDigest;
    }

    public String getCorrectedDigest() {
        return correctedDigest;
    }
}