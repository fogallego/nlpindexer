package es.us.lsi.fogallego.nlpindexer.model.dataset;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.DigestUtils;

import java.util.List;
import java.util.stream.Collectors;

@Document(collection = "datasetelementlite")
public class DataSetElementLite {

    @JsonIgnore
    @Id
    private String id;
    @Indexed
    private String idDataSet;
    private String text;
    @Indexed
    private String textDigest;
    private String corrected;
    private String idMessage;
    private String idRawSentence;
    private int numTokens;
    private List<String> lstFilledTemplate;

    public DataSetElementLite() {
        super();
    }

    public DataSetElementLite(DataSetElementFull dataSetElementFull) {
        this.setIdDataSet(dataSetElementFull.getIdDataSet());
        this.setText(dataSetElementFull.getOriginalText());
        this.setCorrected(dataSetElementFull.getMessage().getText());
        this.setIdMessage(dataSetElementFull.getMessage().getId());
        this.setIdRawSentence(dataSetElementFull.getMessage().getIdRawSentence());
        this.numTokens = dataSetElementFull.getMessage().getLabelledWords().size();

        lstFilledTemplate = dataSetElementFull.getLstMatchedRule()
                .stream()
                .map(mr -> mr.getLstMatching()
                        .stream()
                        .filter(m -> !m.isEmbedded())
                        .map(m -> m.getFilledTemplate())
                        .collect(Collectors.toList()))
                .flatMap(m -> m.stream())
                .collect(Collectors.toList());

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdDataSet() {
        return idDataSet;
    }

    public void setIdDataSet(String idDataSet) {
        this.idDataSet = idDataSet;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        this.textDigest = DigestUtils.md5DigestAsHex(text.getBytes());
    }

    public String getTextDigest() {
        return textDigest;
    }

    public String getCorrected() {
        return corrected;
    }

    public void setCorrected(String corrected) {
        this.corrected = corrected;
    }

    public String getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(String idMessage) {
        this.idMessage = idMessage;
    }

    public String getIdRawSentence() {
        return idRawSentence;
    }

    public void setIdRawSentence(String idRawSentence) {
        this.idRawSentence = idRawSentence;
    }

    public int getNumTokens() {
        return numTokens;
    }

    public void setNumTokens(int numTokens) {
        this.numTokens = numTokens;
    }

    public List<String> getLstFilledTemplate() {
        return lstFilledTemplate;
    }

    public void setLstFilledTemplate(List<String> lstFilledTemplate) {
        this.lstFilledTemplate = lstFilledTemplate;
    }
}
