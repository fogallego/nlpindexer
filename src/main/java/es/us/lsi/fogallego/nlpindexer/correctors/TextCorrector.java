package es.us.lsi.fogallego.nlpindexer.correctors;

import es.us.lsi.fogallego.nlpindexer.exceptions.NLPIndexerApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.CollationKey;
import java.text.Collator;
import java.text.RuleBasedCollator;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class TextCorrector {

    public final String SPACE = "\\s";
    private String regexTok = "([^,\\.;:!¡?¿]+)";
    private String endSentenceTok = ".!?";
    private Pattern patternRegexTok = Pattern.compile(regexTok);

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${dicts.base}")
    private String correctionDictPath;

    @Value("${dicts.whole}")
    private String wholeDictPath;

    private HashSet<String> correctionDict;

    private HashSet<String> wholeDict;

    /**
     * The words are indexed according to their length and their first letter.
     */
    private Map<Integer, Map<String, Map<String, String>>> firstIndex;

    /**
     * The second index.
     */
    private Map<Integer, Map<String, Map<String, String>>> secondIndex;
    /**
     * A cache of corrections for common misspellings.
     */
    private Map<String, List<String>> correctionCache;

    /**
     * The rlb.
     */
    private Collator rlb;

    private Map<Integer, CollationKey> collatorKeys;

    public TextCorrector() {
        firstIndex = new HashMap<Integer, Map<String, Map<String, String>>>();
        secondIndex = new HashMap<Integer, Map<String, Map<String, String>>>();
        correctionCache = new HashMap<String, List<String>>();
        rlb = RuleBasedCollator.getInstance(new Locale("es"));
        rlb.setStrength(Collator.PRIMARY);
        collatorKeys = new HashMap<Integer, CollationKey>();
    }

    private void loadDict() throws IOException {

        if (correctionDict == null) {

            HashSet<String> loadedDict = new HashSet<>(Files.lines(Paths.get(correctionDictPath))
                    .collect(Collectors.toSet()));

            Map<String, Map<String, String>> letterIndex;
            Map<String, String> wordsIndex;

            correctionDict = loadedDict;

            // Create auxiliary index.
            for (String e : correctionDict) {
                // Create first letter auxiliary index.
                letterIndex = firstIndex.get(e.length());
                if (letterIndex == null) {
                    letterIndex = new HashMap<String, Map<String, String>>();
                    firstIndex.put(e.length(), letterIndex);
                }
                wordsIndex = letterIndex.get(e.substring(0, 1));
                if (wordsIndex == null) {
                    wordsIndex = new HashMap<String, String>();
                    letterIndex.put(e.substring(0, 1), wordsIndex);
                }
                wordsIndex.put(e, e);

                // Create second letter auxiliary index.
                if (e.length() >= 2) {
                    letterIndex = secondIndex.get(e.length());
                    if (letterIndex == null) {
                        letterIndex = new HashMap<String, Map<String, String>>();
                        secondIndex.put(e.length(), letterIndex);
                    }
                    wordsIndex = letterIndex.get(e.substring(1, 2));
                    if (wordsIndex == null) {
                        wordsIndex = new HashMap<String, String>();
                        letterIndex.put(e.substring(1, 2), wordsIndex);
                    }
                    wordsIndex.put(e, e);
                }

            }

            correctionDict = loadedDict;
        }

        if (wholeDict == null) {
            wholeDict = new HashSet<>(Files.lines(Paths.get(wholeDictPath))
                    .collect(Collectors.toSet()));
        }
    }

    public boolean hasMajorityUpperCase(String text) {

        int count = 0;
        boolean result = false;
        for (int i = 0; i < text.length() && !result; i++) {
            char c = text.charAt(i);
            if (Character.isUpperCase(c)) {
                count++;
            }
        }

        if (((double) count) / text.length() > 0.75) {
            result = true;
        }


        return result;
    }

    public String correctText(String text) throws NLPIndexerApiException {

        StringBuffer sb = new StringBuffer();

        try {

            loadDict();

            text = fixEndOfText(text);

            String[] splitted = text.split(SPACE);
            for (String token : splitted) {
                String newToken = new String(token);
                Matcher m = patternRegexTok.matcher(token);
                while (m.find()) {
                    for (int i = 0; i < m.groupCount(); i++) {
                        String subToken = m.group(i);
                        String subTokenCorrected = correctToken(subToken);

                        newToken = newToken.replace(subToken, subTokenCorrected);
                    }
                }

                sb.append(newToken);
                sb.append(" ");
            }

            sb.trimToSize();

        } catch (IOException e) {
            log.error("Exception while correcting text: " + text);
            throw new NLPIndexerApiException(e);
        }

        return sb.toString().trim();
    }

    public String fixEndOfText(String text) {
        if (endSentenceTok.indexOf(text.charAt(text.length() - 1)) < 0) {
            text += '.';
        }
        return text;
    }

    /**
     * Check token.
     *
     * @param token the name
     * @return The name normalized
     */
    public String correctToken(final String token) {

        String result = token;

        if (!wholeDict.contains(token.toLowerCase())) {
            final Misspelling miss = getMisspelling(token);
            if (miss != null && miss.getSuggestions() != null && miss.getSuggestions().size() > 0) {
                result = miss.getSuggestions().get(0);
            }
        }

        return result;
    }

    /**
     * Checks whether a word is correct or not and calculates the best
     * corrections.
     *
     * @param token This token contains the word to be corrected.
     * @return A misspelling that contains suggestions to correct the word.
     */
    private Misspelling getMisspelling(final String token) {
        final int lengthDelta = 1;
        Misspelling result;
        String lowerWord;
        Map<String, Map<String, String>> lengthIndex;
        Map<String, String> letterIndex;
        int length;
        int distancia = Integer.MAX_VALUE;
        int dist2;
        String sugest = null;
        result = null;
        lowerWord = token.toLowerCase();
        if (!correctionDict.contains(lowerWord)) {
            result = new Misspelling();
            result.setToken(token);

            if (correctionCache.get(token) == null) {
                result.setSuggestions(new ArrayList<String>());
                correctionCache.put(lowerWord, result.getSuggestions());
                for (length = lowerWord.length() - lengthDelta; length <= lowerWord
                        .length()
                        + lengthDelta; length++) {

                    lengthIndex = firstIndex.get(length);
                    if (lengthIndex != null) {
                        final String letter = String.valueOf(lowerWord
                                .charAt(0));
                        letterIndex = lengthIndex.get(letter);
                        if (letterIndex != null) {
                            for (String entry : letterIndex.values()) {
                                dist2 = similar(lowerWord, entry.toLowerCase());
                                // dist2= getLevenshteinDistance(lowerWord,
                                // letterStr.getWord().toLowerCase());
                                if (dist2 != Integer.MAX_VALUE
                                        && dist2 < distancia) {
                                    distancia = dist2;
                                    sugest = entry;
                                }
                            }
                        }
                    }
                    lengthIndex = secondIndex.get(length + 1);
                    if (lengthIndex != null) {
                        letterIndex = lengthIndex
                                .get(lowerWord.substring(0, 1));
                        if (letterIndex != null) {
                            for (String letterStr : letterIndex.values()) {
                                dist2 = similar(lowerWord, letterStr.toLowerCase());
                                // dist2= getLevenshteinDistance(lowerWord,
                                // letterStr.getWord().toLowerCase());
                                if (dist2 != Integer.MAX_VALUE
                                        && dist2 < distancia) {
                                    distancia = dist2;
                                    sugest = letterStr;
                                }
                            }
                        }
                    }
                }
            } else {
                result.setSuggestions(correctionCache.get(lowerWord));
            }
        }
        if (sugest != null) {
            result.getSuggestions().add(sugest);
        }

        return result;
    }

    /**
     * Determines whether two words are similar or not according to a simple
     * heuristic that is based on the well-known Levenshtein word similarity
     * measure.
     *
     * @param word1 The first word.
     * @param word2 The second word.
     * @return the int
     * @returns The similarity between two words.
     */
    private int similar(final String word1, final String word2) {
        boolean result;
        int distance; // Levenshtein distance
        int[][] matrix; // Levenshtein matrix
        int word1Lenght, word2Lenght; // Length of <c>word1</c> and <c>word2</c>
        int i, j; // Iterates through <c>s</c> and <c>t</c>
        char si, tj; // i-th character of <c>s</c> and <c>t</c>
        int cost; // Edit cost

        result = (Math.abs(word1.length() - word2.length()) <= 2);
        distance = Integer.MAX_VALUE;

        if (result) {
            // Step 1
            word1Lenght = word1.length();
            word2Lenght = word2.length();
            if (word2Lenght == 0) {
                distance = word1Lenght;
            }
            if (word1Lenght == 0) {
                distance = word2Lenght;
            }
            matrix = new int[word2Lenght + 1][word1Lenght + 1];

            if (distance == Integer.MAX_VALUE) {
                // Step 2
                for (i = 0; i <= word2Lenght; i++) {
                    matrix[i][0] = i;
                }
                for (j = 0; j <= word1Lenght; j++) {
                    matrix[0][j] = j;
                }

                // Step 3
                for (i = 1; i <= word2Lenght; i++) {
//					si = word2.substring(i - 1,i);
                    si = word2.charAt(i - 1);
                    // Step 4
                    for (j = 1; j <= word1Lenght; j++) {
//						tj = word1.substring(j - 1,j);
                        tj = word1.charAt(j - 1);
                        // Step 5
                        if (compare(si, tj) == 0/*si == tj*/) {
                            cost = 0;
                        } else {
                            cost = 1;
                        }
                        // Step 6
                        matrix[i][j] = Math.min(Math.min(matrix[i - 1][j] + 1,
                                matrix[i][j - 1] + 1), matrix[i - 1][j - 1]
                                + cost);
                    }
                }

                // Step 7
                distance = matrix[word2Lenght][word1Lenght];
            }
        }

        result = result && (word1.length() > 4 || distance <= 1)
                && (word1.length() > 8 || distance <= 2)
                && (word1.length() <= 8 || distance <= 3);

        if (result) {
            return distance;
        } else {
            return Integer.MAX_VALUE;
        }

    }

    /**
     * @param si
     * @param tj
     * @return
     */
    private int compare(char si, char tj) {

        CollationKey key1 = collatorKeys.get((int) si);
        CollationKey key2 = collatorKeys.get((int) tj);

        if (key1 == null) {
            key1 = rlb.getCollationKey(String.valueOf(si));
            collatorKeys.put((int) si, key1);
        }
        if (key2 == null) {
            key2 = rlb.getCollationKey(String.valueOf(tj));
            collatorKeys.put((int) tj, key2);
        }

        return key1.compareTo(key2);
    }

}
