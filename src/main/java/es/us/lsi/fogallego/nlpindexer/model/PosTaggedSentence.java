package es.us.lsi.fogallego.nlpindexer.model;

import es.us.lsi.fogallego.nlpindexer.patternextraction.UtilPatternExtraction;

import java.util.List;

public class PosTaggedSentence {

    private String text;
    private List<WordToken> lstWordToken;

    public PosTaggedSentence(String text, List<WordToken> lstWordToken) {
        this.text = text;
        this.lstWordToken = lstWordToken;
    }

    public PosTaggedSentence(String text, String lstWordTokenStr) {
        this(text, UtilPatternExtraction.getListWordToken(lstWordTokenStr));
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<WordToken> getLstWordToken() {
        return lstWordToken;
    }

    public void setLstWordToken(List<WordToken> lstWordToken) {
        this.lstWordToken = lstWordToken;
    }

}
