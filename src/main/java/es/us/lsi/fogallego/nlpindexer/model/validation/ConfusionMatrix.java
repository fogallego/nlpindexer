package es.us.lsi.fogallego.nlpindexer.model.validation;

import java.math.BigInteger;

public class ConfusionMatrix {

    private long truePositive;
    private long falsePositive;
    private BigInteger trueNegative;
    private long falseNegative;

    public ConfusionMatrix() {
        this.truePositive = 0;
        this.falsePositive = 0;
        this.trueNegative = new BigInteger("0");
        this.falseNegative = 0;
    }

    public ConfusionMatrix(ConfusionMatrix confusionMatrix) {
        this.truePositive = confusionMatrix.getTruePositive();
        this.falsePositive = confusionMatrix.getFalsePositive();
        this.trueNegative = confusionMatrix.getTrueNegative();
        this.falseNegative = confusionMatrix.getFalseNegative();
    }

    public ConfusionMatrix(long truePositive, long falsePositive, BigInteger trueNegative, long falseNegative) {
        this.truePositive = truePositive;
        this.falsePositive = falsePositive;
        this.trueNegative = trueNegative;
        this.falseNegative = falseNegative;
    }

    public ConfusionMatrix aggregate(ConfusionMatrix confusionMatrix) {
        this.truePositive += confusionMatrix.getTruePositive();
        this.falsePositive += confusionMatrix.getFalsePositive();
        this.trueNegative = this.trueNegative.add(confusionMatrix.getTrueNegative());
        this.falseNegative += confusionMatrix.getFalseNegative();

        return this;
    }

    public ConfusionMatrix aggregate(int truePositive, int falsePositive, BigInteger trueNegative, int falseNegative) {
        this.truePositive += truePositive;
        this.falsePositive += falsePositive;
        this.trueNegative = this.trueNegative.add(trueNegative);
        this.falseNegative += falseNegative;

        return this;
    }

    public long getTruePositive() {
        return truePositive;
    }

    public void setTruePositive(long truePositive) {
        this.truePositive = truePositive;
    }

    public long getFalsePositive() {
        return falsePositive;
    }

    public void setFalsePositive(long falsePositive) {
        this.falsePositive = falsePositive;
    }

    public BigInteger getTrueNegative() {
        return trueNegative;
    }

    public void setTrueNegative(BigInteger trueNegative) {
        this.trueNegative = trueNegative;
    }

    public long getFalseNegative() {
        return falseNegative;
    }

    public void setFalseNegative(long falseNegative) {
        this.falseNegative = falseNegative;
    }

    @Override
    public String toString() {
        return "ConfusionMatrix{" +
                "truePositive=" + truePositive +
                ", falsePositive=" + falsePositive +
                ", trueNegative=" + trueNegative.toString() +
                ", falseNegative=" + falseNegative +
                '}';
    }
}
