package es.us.lsi.fogallego.nlpindexer.controllers;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import es.us.lsi.fogallego.nlpindexer.correctors.TextCorrector;
import es.us.lsi.fogallego.nlpindexer.dao.impl.MessageDaoCustomImpl;
import es.us.lsi.fogallego.nlpindexer.exceptions.NLPIndexerApiException;
import es.us.lsi.fogallego.nlpindexer.model.Message;
import es.us.lsi.fogallego.nlpindexer.model.PosTaggedSentence;
import es.us.lsi.fogallego.nlpindexer.postagging.PosTagger;
import org.apache.catalina.connector.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@Api("postagger")
@RestController
@RequestMapping(PathVariables.API+"postagger")
public class PosTaggerController {

    protected Logger log	= LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TextCorrector textCorrector;

    @Autowired
    private PosTagger posTagger;

    @Autowired
    private MessageDaoCustomImpl messageDaoCustom;

    @ApiOperation(value = "Postag one text with NLP annotations")
    @ApiResponses({@ApiResponse(code = Response.SC_BAD_REQUEST, message = "IOException while processing file")})
    @RequestMapping(method = RequestMethod.POST)
    public List<Message> posTagText(@RequestParam String text) throws NLPIndexerApiException {
        log.debug("Start indexing of text: "+ text);

        try {
            String corrected = textCorrector.correctText(text);
            List<PosTaggedSentence> lstSentence = posTagger.getSentences(corrected);
            List<Message> lstMessage = lstSentence.stream()
                    .map(sent -> messageDaoCustom.convert2Message(sent))
                    .collect(Collectors.toList());

            return lstMessage;
        } catch (Exception e) {
            log.error("Error while text correction", e);
            throw new NLPIndexerApiException(e);
        }
    }

}
