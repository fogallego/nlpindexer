package es.us.lsi.fogallego.nlpindexer.patternextraction;

import es.us.lsi.fogallego.nlpindexer.model.LabelledWord;
import es.us.lsi.fogallego.nlpindexer.model.LabelledWordParent;
import es.us.lsi.fogallego.nlpindexer.model.WordToken;
import es.us.lsi.fogallego.nlpindexer.model.enums.EnumOwnTag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class UtilPatternExtraction {

    private static final Pattern p = Pattern.compile("\\{\\s(\\S+)\\s(\\S+)\\s(\\S+)\\s\\}");
    public static final String DELIMITER = "|";

    public static final String BASIC_NOUN_PHRASE = "bNP";
    private static final int MAX_PATTERN_SIZE = 8;

    private static final String[] basicNounPhrasePatterns =
            {
                    // basic nounphrases
                    "NOUN",
                    "NOUN-NOUN",
                    "NOUN-ADJ",
                    "ADJ-NOUN",
                    "NOUN-PP",
                    "PP-NOUN",

                    // new basic nounphrases
                    "ADJ-NOUN-ADJ",
                    "ADJ-NOUN-PP",
                    "NOUN-ADJ-ADJ",
                    "NOUN-ADJ-NOUN",
                    "NOUN-NOUN-ADJ",
                    "PP-NOUN-ADJ",
                    "PP-NOUN-NOUN",
                    "NOUN-ADJ-ADJ",
                    "NOUN-PP-ADJ",
                    "NOUN-PP-NOUN",

                    /*
                    {ADJ,NOUN,NOUN} -> too much noise
                    {NOUN,ADJ,PP} -> so much noise
                     */

                    // basic nounphrases with DET
                    "DET-NOUN",
                    "DET-ADJ",
                    "DET-NOUN-NOUN",
                    "DET-NOUN-ADJ",
                    "DET-ADJ-NOUN",
                    "DET-NOUN-PP",
                    "DET-PP-NOUN",

                    // new basic nounphrases with DET
                    "DET-ADJ-NOUN-ADJ",
                    "DET-ADJ-NOUN-PP",
                    "DET-NOUN-ADJ-ADJ",
                    "DET-NOUN-ADJ-NOUN",
                    "DET-NOUN-NOUN-ADJ",
                    "DET-PP-NOUN-ADJ",
                    "DET-PP-NOUN-NOUN",
                    "DET-NOUN-ADJ-ADJ",
                    "DET-NOUN-PP-ADJ",
                    "DET-NOUN-PP-NOUN"
            };

    private static final String[] nounPhrasePatterns = {
            BASIC_NOUN_PHRASE,
            BASIC_NOUN_PHRASE + "-PREP-" + BASIC_NOUN_PHRASE,
            BASIC_NOUN_PHRASE + "-PREP-" + BASIC_NOUN_PHRASE + "-PREP-" + BASIC_NOUN_PHRASE,

            //With adj
            "ADJ-PREP-" + BASIC_NOUN_PHRASE,
            BASIC_NOUN_PHRASE + "-PREP-ADJ",

            "ADJ-PREP-" + BASIC_NOUN_PHRASE + "-PREP-" + BASIC_NOUN_PHRASE,
            BASIC_NOUN_PHRASE + "-PREP-ADJ-PREP-" + BASIC_NOUN_PHRASE,
            BASIC_NOUN_PHRASE + "-PREP-" + BASIC_NOUN_PHRASE + "-PREP-ADJ",

            "ADJ-PREP-ADJ-PREP-" + BASIC_NOUN_PHRASE,
            BASIC_NOUN_PHRASE + "-PREP-ADJ-PREP-ADJ",
            "ADJ-PREP-" + BASIC_NOUN_PHRASE + "-PREP-ADJ",
    };
    public static final String DELIMITER_SPACE = " ";

    private static String[] arrayIs = {"ser"};
    private static String[] arrayHavePos = {"tener", "poseer", "contener", "albergar"};
    private static String[] arrayHaveAux = {"haber"};


    private static final HashSet<String> lstBasicNounPhrasePatterns = new HashSet(Arrays.asList(basicNounPhrasePatterns));
    private static final HashSet<String> lstNounPhrasePatterns = new HashSet(Arrays.asList(nounPhrasePatterns));
    private static HashSet<String> lstIs = new HashSet<>(Arrays.asList(arrayIs));
    private static HashSet<String> lstHavePos = new HashSet<>(Arrays.asList(arrayHavePos));
    private static HashSet<String> lstHaveAux = new HashSet<>(Arrays.asList(arrayHaveAux));

    public static List<WordToken> getListWordToken(String strWordTokens) {

        List<WordToken> lstWordToken = new ArrayList<>();
        Matcher m = p.matcher(strWordTokens);
        while (m.find()) {
            WordToken wordToken = new WordToken(m.group(1), m.group(2), m.group(3));
            lstWordToken.add(wordToken);
        }

        return lstWordToken;
    }

    public static EnumOwnTag standardTag(String lemma, String tag) {

        EnumOwnTag standardTag = EnumOwnTag.OTHER;
        char cat = tag.charAt(0);

        if (cat == 'N') {
//        if (java.util.regex.Pattern.matches("N[C|P][0-9a-zA-Z]+", tag)) {
            standardTag = EnumOwnTag.NOUN;
        } else if (cat == 'A') {
//        else if (java.util.regex.Pattern.matches("A[Q|O][0-9a-zA-Z]+", tag)) {
            standardTag = EnumOwnTag.ADJ;
        } else if (cat == 'V') {
//            if (java.util.regex.Pattern.matches("V[0-9a-zA-Z]+", tag)) {
            if (Pattern.matches("V[M|S]P[0-9a-zA-Z]+", tag)) {
                standardTag = EnumOwnTag.PP;
            } else if (lstIs.contains(lemma) && Pattern.matches("VSIP[0-9a-zA-Z]+",tag)) {
                standardTag = EnumOwnTag.IS;
            } else if (lstHavePos.contains(lemma)) {
                standardTag = EnumOwnTag.HAVEPOS;
            } else if (lstHaveAux.contains(lemma)) {
                standardTag = EnumOwnTag.HAVEAUX;
            } else {
                standardTag = EnumOwnTag.VERB;
            }
//            }
        } else if (cat == 'R') {
//        else if (Pattern.matches("R[G|N]", tag)) {
            standardTag = EnumOwnTag.ADVERB;
        } else if (cat == 'S') {
//        } else if (Pattern.matches("SP[S|C][0|M][0|S]", tag)) {
            standardTag = EnumOwnTag.PREP;
        } else if (cat == 'C') {
//        } else if (Pattern.matches("C[C|S]", tag)) {
            standardTag = EnumOwnTag.CONJ;
        } else if (cat == 'P') {
//        } else if (Pattern.matches("P[0-9a-zA-Z]+", tag)) {
            standardTag = EnumOwnTag.PRONOUN;
        } else if (cat == 'I') {
//        } else if (Pattern.matches("I", tag)) {
            standardTag = EnumOwnTag.INT;
        } else if (cat == 'F') {
            if (Pattern.matches("Fp", tag)) {
                standardTag = EnumOwnTag.POINT;
            } else if (Pattern.matches("Fc", tag)) {
                standardTag = EnumOwnTag.COMMA;
            } else if (Pattern.matches("F[0-9a-zA-Z]+", tag)) {
                standardTag = EnumOwnTag.OTHERPUNT;
            }
        } else if (cat == 'D') {
//        } else if (Pattern.matches("D[0-9a-zA-Z]+", tag)) {
            standardTag = EnumOwnTag.DET;
        } else if (cat == 'E') {
//        } else if (Pattern.matches("D[0-9a-zA-Z]+", tag)) {
            standardTag = EnumOwnTag.EM;
        }

        return standardTag;
    }

    public static List<LabelledWordParent> groupLabelledWord(List<LabelledWordParent> lstLabelledWord) {
        List<LabelledWordParent> lstWordTokenBNP = groupLabelledWord(lstBasicNounPhrasePatterns, EnumOwnTag.bNP, lstLabelledWord);

        return groupLabelledWord(lstNounPhrasePatterns, EnumOwnTag.NP, lstWordTokenBNP);
    }

    private static List<LabelledWordParent> groupLabelledWord(HashSet<String> lstPatterns, EnumOwnTag tag, List<LabelledWordParent> lstLabelledWord) {
        List<LabelledWordParent> lstGroupedLabelledWord = new ArrayList<>();

        for (int i = 0; i < lstLabelledWord.size(); i++) {
            int limitJ = Math.min(i + MAX_PATTERN_SIZE, lstLabelledWord.size());
            for (int j = limitJ; j > i; j--) {
                List<LabelledWordParent> subList = lstLabelledWord.subList(i, j);
                if (matchWithPatternList(subList, lstPatterns)) {

                    //TODO improve
                    List<LabelledWord> subListAux = new ArrayList<>(subList);
                    if (tag.equals(EnumOwnTag.NP)) {
                        List<LabelledWord> tempLabelledWordParentList = new ArrayList<>();
                        subList.forEach(lwp -> {
                                    if (lwp.getOwnTag().equals(EnumOwnTag.bNP)) {
                                        tempLabelledWordParentList.addAll(lwp.getSubLabelledWordList());
                                    } else {
                                        tempLabelledWordParentList.add(lwp);
                                    }
                                }
                        );
                        subListAux = tempLabelledWordParentList;
                    }

                    lstGroupedLabelledWord.add(groupSubList(tag, subListAux));

                    // Fix indexes
                    i = j - 1;
                    break;
                } else if (j - i == 1) {
                    // Isolated token
                    lstGroupedLabelledWord.add(lstLabelledWord.get(i));
                }
            }
        }
        fixPosition(lstGroupedLabelledWord);
        return lstGroupedLabelledWord;
    }

    private static void fixPosition(List<LabelledWordParent> lstGroupedLabelledWord) {
        int position = 1;
        for (LabelledWordParent labelledWordParent : lstGroupedLabelledWord) {
            labelledWordParent.setPosition(position);
            position++;
        }
    }

    /**
     * This method create a LabelledWordParent with the grouped tokens of subList.
     *
     * @param tag     tag assigned to the group
     * @param subList subList to be grouped
     * @return The grouped token as LabelledWordParent
     */
    private static LabelledWordParent groupSubList(EnumOwnTag tag, List<LabelledWord> subList) {
        LabelledWordParent groupedLabelledWord = new LabelledWordParent();
        groupedLabelledWord.setToken(String.join(DELIMITER_SPACE, subList.stream().map(LabelledWord::getToken).collect(Collectors.toList())));
        groupedLabelledWord.setLemma(String.join(DELIMITER_SPACE, subList.stream().map(LabelledWord::getLemma).collect(Collectors.toList())));
        groupedLabelledWord.setEagle(String.join(DELIMITER_SPACE, subList.stream().map(LabelledWord::getEagle).collect(Collectors.toList())));
        groupedLabelledWord.setOwnTag(tag);
        // Criteria used: if majority is idiom, the group is idiom.
        groupedLabelledWord.setIdiom(subList.stream().map(lw -> lw.isIdiom()).filter(p -> p == true).count() > subList.size() / 2);
        groupedLabelledWord.setPosition(subList.get(0).getPosition());
//        if (tag.equals(EnumOwnTag.bNP) || subList.size() > 1) {
        List<LabelledWord> newSubList = new ArrayList<>(subList);
        int newPosition = 1;
        for (LabelledWord lw : newSubList) {
            lw.setPosition(newPosition);
            newPosition++;
        }
        groupedLabelledWord.setSubLabelledWordList(newSubList);
//        }

        return groupedLabelledWord;
    }

    /**
     *
     * Analyze if subList represents any special type of nounphrase.
     *
     * @param subList of nounphrase group
     * @return the final tag
     */
    @Deprecated
    private static EnumOwnTag getNounPhraseType(List<LabelledWord> subList) {

        EnumOwnTag result = EnumOwnTag.NP;

        if (subList.stream().map(lw -> {
                if (lw.getOwnTag().equals(EnumOwnTag.PREP)) {
                    if (lw.getLemma().equals("de")) {
                        return true;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            }).filter(res -> res != null)
              .anyMatch(res -> res == true)) {
                    result = EnumOwnTag.NP_OF;
        }

        return result;
    }

    private static boolean matchWithPatternList(List<LabelledWordParent> labelledWord, HashSet<String> lstPattern) {

        String pattern = getPatternFromListLabelledWord(labelledWord);

        return lstPattern.contains(pattern);
    }

    public static String getPatternFromListLabelledWord(List<LabelledWordParent> lstLabelledWord) {
        return lstLabelledWord.stream().map(lw -> lw.getOwnTag().name())
                .collect(Collectors.joining("-"));
    }

    public static String getInstanceFromListWordToken(List<WordToken> lstWordToken) {
        return lstWordToken.stream().map(WordToken::getOriginal)
                .collect(Collectors.joining(DELIMITER));
    }

}