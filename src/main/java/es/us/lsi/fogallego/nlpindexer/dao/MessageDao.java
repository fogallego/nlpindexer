package es.us.lsi.fogallego.nlpindexer.dao;

import es.us.lsi.fogallego.nlpindexer.model.Message;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface MessageDao extends MongoRepository<Message, String> {

    public Message findMessageById(String id);
//    public List<Message> findMessageByIndexedTagging(String indexedTagging);

}
