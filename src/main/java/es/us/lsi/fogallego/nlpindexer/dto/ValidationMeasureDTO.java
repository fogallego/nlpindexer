package es.us.lsi.fogallego.nlpindexer.dto;

public class ValidationMeasureDTO {

    private String measureName;
    private Double value;

    public ValidationMeasureDTO(String measureName, Double value) {
        this.measureName = measureName;
        this.value = value;
    }

    public String getMeasureName() {
        return measureName;
    }

    public void setMeasureName(String measureName) {
        this.measureName = measureName;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
