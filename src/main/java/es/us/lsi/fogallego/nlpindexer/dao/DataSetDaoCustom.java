package es.us.lsi.fogallego.nlpindexer.dao;

import org.springframework.stereotype.Component;

@Component
public interface DataSetDaoCustom {

    public boolean existsDataSetWithName(String name);

}
