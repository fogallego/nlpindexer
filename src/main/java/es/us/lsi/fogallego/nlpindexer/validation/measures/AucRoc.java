package es.us.lsi.fogallego.nlpindexer.validation.measures;

import es.us.lsi.fogallego.nlpindexer.model.validation.ConfusionMatrix;
import es.us.lsi.fogallego.nlpindexer.validation.AbstractValidationMeasure;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class AucRoc extends AbstractValidationMeasure {
    private static final String AUCROC = "AUC-ROC";

    @Override
    protected String measureName() {
        return AUCROC;
    }

    @Override
    protected Double calculateWithMicroAverage(ConfusionMatrix confusionMatrix) {
        //  0.5 * (TPR - FPR + 1) = 0.5 * (TP / (TP + FN) – FP / (FP + TN) + 1)
        Double tpr;

        if (confusionMatrix.getTruePositive() == 0 && confusionMatrix.getFalseNegative() == 0) {
            tpr = 0d;
        } else {
            tpr = ((double) confusionMatrix.getTruePositive()) / (confusionMatrix.getTruePositive() +
                    confusionMatrix.getFalseNegative());
        }

        Double fpr;

        if (confusionMatrix.getFalsePositive() == 0 && confusionMatrix.getTrueNegative().signum() == 0) {
            fpr = 0d;
        } else {
            BigDecimal fpBigInt = new BigDecimal(String.valueOf(confusionMatrix.getFalsePositive()));
            BigDecimal trueNegative = new BigDecimal(confusionMatrix.getTrueNegative().toString());
            fpr = fpBigInt.divide(trueNegative.add(fpBigInt), RoundingMode.HALF_UP).doubleValue();
        }

        return 0.5 * (tpr - fpr + 1);
    }
}
