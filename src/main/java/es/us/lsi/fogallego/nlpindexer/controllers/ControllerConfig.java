package es.us.lsi.fogallego.nlpindexer.controllers;

import es.us.lsi.fogallego.nlpindexer.exceptions.NLPIndexerApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The Class ControllerConfig.
 */
@ControllerAdvice
public class ControllerConfig {

	/** The log. */
	protected Logger	log	= LoggerFactory.getLogger(this.getClass());

	/**
	 * Handle bad requests.
	 *
	 * @param exception the exception
	 * @param response the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@ExceptionHandler({NLPIndexerApiException.class})
	public void handleBadRequests(NLPIndexerApiException exception, HttpServletResponse response) throws IOException {
		response.sendError(exception.getResponseStatusCode());
	}

}
