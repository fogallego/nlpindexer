package es.us.lsi.fogallego.nlpindexer.model.dataset;

import es.us.lsi.fogallego.nlpindexer.model.util.Pair;

import java.util.List;

public class Matching {

    private String filledTemplate;
    private List<Pair<Integer,Integer>> lstTokenRange;
    private boolean embedded;

    public Matching(String filledTemplate, List<Pair<Integer, Integer>> lstTokenRange) {
        this.filledTemplate = filledTemplate;
        this.lstTokenRange = lstTokenRange;
        this.embedded = false;
    }

    public String getFilledTemplate() {
        return filledTemplate;
    }

    public void setFilledTemplate(String filledTemplate) {
        this.filledTemplate = filledTemplate;
    }

    public List<Pair<Integer, Integer>> getLstTokenRange() {
        return lstTokenRange;
    }

    public void setLstTokenRange(List<Pair<Integer, Integer>> lstTokenRange) {
        this.lstTokenRange = lstTokenRange;
    }

    public boolean isEmbedded() {
        return embedded;
    }

    public void setEmbedded(boolean embedded) {
        this.embedded = embedded;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Matching)) return false;

        Matching matching = (Matching) o;

        if (embedded != matching.embedded) return false;
        if (filledTemplate != null ? !filledTemplate.equals(matching.filledTemplate) : matching.filledTemplate != null)
            return false;
        return lstTokenRange != null ? lstTokenRange.equals(matching.lstTokenRange) : matching.lstTokenRange == null;

    }

    @Override
    public int hashCode() {
        int result = filledTemplate != null ? filledTemplate.hashCode() : 0;
        result = 31 * result + (lstTokenRange != null ? lstTokenRange.hashCode() : 0);
        result = 31 * result + (embedded ? 1 : 0);
        return result;
    }
}
