package es.us.lsi.fogallego.nlpindexer.correctors;

import java.util.List;

public class Misspelling {

    /** The token. */
    private String token;

    /** The suggestions. */
    private List<String> suggestions;

    /**
     * Instantiates a new misspelling.
     */
    public Misspelling() {
        this.token = null;
        this.suggestions = null;
    }

    /**
     * Gets the token.
     *
     * @return the token
     */
    public String getToken() {
        return this.token;
    }

    /**
     * Sets the token.
     *
     * @param token the new token
     */
    public void setToken(final String token) {
        this.token = token;
    }

    /**
     * Gets the suggestions.
     *
     * @return the suggestions
     */
    public List<String> getSuggestions() {
        return this.suggestions;
    }

    /**
     * Sets the suggestions.
     *
     * @param suggestions the new suggestions
     */
    public void setSuggestions(final List<String> suggestions) {
        this.suggestions = suggestions;
    }

}
