package es.us.lsi.fogallego.nlpindexer.dto;

import com.wordnik.swagger.annotations.ApiModel;

import java.util.List;

@ApiModel
public class MessageDTO {

    private String text;
    private NLPTagDTO nlpTag;
    private List<LabelledWordDTO> labelledWords;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public NLPTagDTO getNlpTag() {
        return nlpTag;
    }

    public void setNlpTag(NLPTagDTO nlpTag) {
        this.nlpTag = nlpTag;
    }

    public List<LabelledWordDTO> getLabelledWords() {
        return labelledWords;
    }

    public void setLabelledWords(List<LabelledWordDTO> labelledWords) {
        this.labelledWords = labelledWords;
    }
}
