package es.us.lsi.fogallego.nlpindexer.dto;

import java.util.List;

public class DataSetValidationDTO {

    private String idDataSet;
    private long dataSetSize;
    private int goldenRuleSize;
    private int numDataSetElementsInGoldenRule;

    private List<ValidationMeasureDTO> lstValidationMeasureMacroAverage;
    private List<ValidationMeasureDTO> lstValidationMeasureMicroAverage;

    public String getIdDataSet() {
        return idDataSet;
    }

    public void setIdDataSet(String idDataSet) {
        this.idDataSet = idDataSet;
    }

    public long getDataSetSize() {
        return dataSetSize;
    }

    public void setDataSetSize(long dataSetSize) {
        this.dataSetSize = dataSetSize;
    }

    public int getGoldenRuleSize() {
        return goldenRuleSize;
    }

    public void setGoldenRuleSize(int goldenRuleSize) {
        this.goldenRuleSize = goldenRuleSize;
    }

    public int getNumDataSetElementsInGoldenRule() {
        return numDataSetElementsInGoldenRule;
    }

    public void setNumDataSetElementsInGoldenRule(int numDataSetElementsInGoldenRule) {
        this.numDataSetElementsInGoldenRule = numDataSetElementsInGoldenRule;
    }

    public List<ValidationMeasureDTO> getLstValidationMeasureMacroAverage() {
        return lstValidationMeasureMacroAverage;
    }

    public void setLstValidationMeasureMacroAverage(List<ValidationMeasureDTO> lstValidationMeasureMacroAverage) {
        this.lstValidationMeasureMacroAverage = lstValidationMeasureMacroAverage;
    }

    public List<ValidationMeasureDTO> getLstValidationMeasureMicroAverage() {
        return lstValidationMeasureMicroAverage;
    }

    public void setLstValidationMeasureMicroAverage(List<ValidationMeasureDTO> lstValidationMeasureMicroAverage) {
        this.lstValidationMeasureMicroAverage = lstValidationMeasureMicroAverage;
    }
}
