package es.us.lsi.fogallego.nlpindexer.model;

import es.us.lsi.fogallego.nlpindexer.model.LabelledWord;

import java.util.List;

public class LabelledWordParent extends LabelledWord {

    private List<LabelledWord> subLabelledWordList;

    public List<LabelledWord> getSubLabelledWordList() {
        return subLabelledWordList;
    }

    public void setSubLabelledWordList(List<LabelledWord> subLabelledWordList) {
        this.subLabelledWordList = subLabelledWordList;
    }
}
