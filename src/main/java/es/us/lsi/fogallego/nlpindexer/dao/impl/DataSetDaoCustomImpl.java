package es.us.lsi.fogallego.nlpindexer.dao.impl;

import es.us.lsi.fogallego.nlpindexer.dao.DataSetDaoCustom;
import es.us.lsi.fogallego.nlpindexer.model.dataset.DataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

@Component
public class DataSetDaoCustomImpl implements DataSetDaoCustom {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public boolean existsDataSetWithName(String name) {

        Criteria criteria = Criteria.where("datasetConfiguration.output.name")
                .is(name);

        return mongoTemplate.exists(new Query(criteria), DataSet.class);
    }
}
