package es.us.lsi.fogallego.nlpindexer.dao;

import es.us.lsi.fogallego.nlpindexer.model.dataset.DataSetElementLite;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface DataSetElementLiteDao extends MongoRepository<DataSetElementLite, String> {

    public List<DataSetElementLite> findByIdDataSet(String idDataSet);

    public List<DataSetElementLite> findByIdDataSet(String idDataSet, Pageable pageable);

    public Long deleteDataSetElementByIdDataSet(String idDataSet);

    public Long countByIdDataSet(String idDataSet);
}
