package es.us.lsi.fogallego.nlpindexer.controllers;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import es.us.lsi.fogallego.nlpindexer.correctors.TextCorrector;
import es.us.lsi.fogallego.nlpindexer.dao.MessageDaoCustom;
import es.us.lsi.fogallego.nlpindexer.dao.RawSentenceDao;
import es.us.lsi.fogallego.nlpindexer.exceptions.NLPIndexerApiException;
import es.us.lsi.fogallego.nlpindexer.languagedetector.LanguageDetectorAdapter;
import es.us.lsi.fogallego.nlpindexer.model.Message;
import es.us.lsi.fogallego.nlpindexer.model.PosTaggedSentence;
import es.us.lsi.fogallego.nlpindexer.model.RawSentence;
import es.us.lsi.fogallego.nlpindexer.postagging.PosTagger;
import org.apache.catalina.connector.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Api("indexer")
@RestController
@RequestMapping(PathVariables.API+"indexer")
public class IndexerController {

    public static final String OK = "ok";
    public static final int PAGE_LIMIT = 1000;
    protected Logger log	= LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MessageDaoCustom messageDaoCustom;

    @Autowired
    private TextCorrector textCorrector;

    @Autowired
    private PosTagger posTagger;

    @Autowired
    private LanguageDetectorAdapter lAdapter;

    @Autowired
    private RawSentenceDao rawSentenceDao;

    @ApiOperation(value = "Index one text with NLP annotations")
    @ApiResponses({@ApiResponse(code = Response.SC_BAD_REQUEST, message = "IOException while processing file")})
    @RequestMapping(value = "/rawimporter", method = RequestMethod.POST)
    public String indexFromRawSentence() throws NLPIndexerApiException {

        log.debug("Start indexing from collection rawsentence");

        int pageNumber = 0;

        Page<RawSentence> page;

        do {

            page = rawSentenceDao.findAll(new PageRequest(++pageNumber, PAGE_LIMIT));

            page.forEach(sent -> processAndSaveFromRaw(sent));

        } while (!page.isLast());

        log.debug("Index finished");

        return OK;
    }

    @ApiOperation(value = "Index one text with NLP annotations")
    @ApiResponses({@ApiResponse(code = Response.SC_BAD_REQUEST, message = "IOException while processing file")})
    @RequestMapping(method = RequestMethod.POST)
    public List<Message> indexText(@RequestParam String text) throws NLPIndexerApiException {

        log.debug("Start indexing of text: "+ text);

        return getAndIndexMessagesFromText(text);
    }

    @Deprecated
    @ApiOperation(value = "Index from file with NLP annotations")
    @ApiResponses({@ApiResponse(code = Response.SC_BAD_REQUEST, message = "IOException while processing file")})
    @RequestMapping(value = "/importer", method = RequestMethod.POST)
    public String indexFromFile(@RequestParam String filePath) throws NLPIndexerApiException {

        log.warn("Start indexing from file: "+filePath);

        try {
            Files.lines(Paths.get(filePath))
//                    .collect(Collectors.toList())
//                    .parallelStream()
                .forEach(line -> {
                    try {
                        getAndIndexMessagesFromText(line);
                    } catch (NLPIndexerApiException e) {
                        log.error("Error with line: "+line, e);
                    }
                });
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new NLPIndexerApiException(e);
        }

        return OK;
    }

    private void processAndSaveFromRaw(final RawSentence rawSentence) {
        List<PosTaggedSentence> lstSentence = posTagger.getSentences(rawSentence.getCorrected());
        lstSentence.forEach(sent -> messageDaoCustom.indexMessageFromSentence(sent,rawSentence.getId()));
    }

    private List<Message> getAndIndexMessagesFromText(@RequestParam String text) throws NLPIndexerApiException {
        try {

            String textCleaned = text.replaceAll("\\p{Cntrl}", "");

            boolean majorityUpperCase = textCorrector.hasMajorityUpperCase(textCleaned);
            boolean probablySpanish = lAdapter.isProbablySpanish(textCleaned.toLowerCase());

            if (!majorityUpperCase && probablySpanish) {
                String corrected = textCorrector.correctText(textCleaned);
                return getMessages(corrected);
            } else {
                log.warn("MajorityUpperCase: " + majorityUpperCase + " ProbablySpanish: " + probablySpanish
                        + " Text:" + text);
                return new ArrayList<>();
            }
        } catch (NLPIndexerApiException e) {
            log.error("NLPIndexerApiException: ", e);
            throw e;
        } catch (Exception e) {
            log.error("Other exception ", e);
            throw new NLPIndexerApiException(e);
        }
    }

    private List<Message> getMessages(String text) {
        List<PosTaggedSentence> lstSentence = posTagger.getSentences(text);
        List<Message> lstMessage = lstSentence.stream()
                .map(sent -> messageDaoCustom.indexMessageFromSentence(sent))
                .collect(Collectors.toList());

        return lstMessage;
    }

}
