package es.us.lsi.fogallego.nlpindexer.model.enums;

public enum     EnumOwnTag {

    NOUN, ADJ, PP, IS, HAVEPOS, HAVEAUX, VERB, ADVERB, PREP,
    CONJ, PRONOUN, INT, POINT, COMMA, OTHERPUNT, DET, EM, bNP, NP, NP_OF, OTHER, INVALID;
}
