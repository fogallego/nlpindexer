package es.us.lsi.fogallego.nlpindexer.controllers;

/**
 * The Class PathVariables.
 */
public class PathVariables {

	/** The Constant API_VERSION. */
	public final static String	API_VERSION	= "1";

	/** The Constant API. */
	public final static String	API			= "/api/v" + API_VERSION + "/";

}
