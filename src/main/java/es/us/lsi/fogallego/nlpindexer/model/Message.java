package es.us.lsi.fogallego.nlpindexer.model;

import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "message")
public class Message {

    @Id
    private String id;
    private String text;
    private String annotatedText;
    private NLPTag nlpTag;
    @Indexed
    private String idRawSentence;
    private List<LabelledWordParent> labelledWords;
    private List<LabelledWordParent> labelledWordsGrouped;
    @CreatedDate
    private DateTime creationDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAnnotatedText() {
        return annotatedText;
    }

    public void setAnnotatedText(String annotatedText) {
        this.annotatedText = annotatedText;
    }

    public NLPTag getNlpTag() {
        return nlpTag;
    }

    public void setNlpTag(NLPTag nlpTag) {
        this.nlpTag = nlpTag;
    }

    public List<LabelledWordParent> getLabelledWords() {
        return labelledWords;
    }

    public void setLabelledWords(List<LabelledWordParent> labelledWords) {
        this.labelledWords = labelledWords;
    }

    public List<LabelledWordParent> getLabelledWordsGrouped() {
        return labelledWordsGrouped;
    }

    public void setLabelledWordsGrouped(List<LabelledWordParent> labelledWordsGrouped) {
        this.labelledWordsGrouped = labelledWordsGrouped;
    }

    public DateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(DateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getIdRawSentence() {
        return idRawSentence;
    }

    public void setIdRawSentence(String idRawSentence) {
        this.idRawSentence = idRawSentence;
    }
}
