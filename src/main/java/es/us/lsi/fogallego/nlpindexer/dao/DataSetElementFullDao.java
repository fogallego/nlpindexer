package es.us.lsi.fogallego.nlpindexer.dao;

import es.us.lsi.fogallego.nlpindexer.model.dataset.DataSetElementFull;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface DataSetElementFullDao extends MongoRepository<DataSetElementFull, String> {

    public List<DataSetElementFull> findByIdDataSet(String idDataSet);

    public List<DataSetElementFull> findByIdDataSet(String idDataSet, Pageable pageable);

    public Long deleteDataSetElementByIdDataSet(String idDataSet);

    public Long countByIdDataSet(String idDataSet);
}
