package es.us.lsi.fogallego.nlpindexer.exceptions;

/**
 * The Class NLPIndexerApiException.
 */
public class NLPIndexerApiException extends ApiException {

    /**
     * Instantiates a new NLPIndexer api exception.
     */
    public NLPIndexerApiException() {
        super();
    }

    /**
     * Instantiates a new NLPIndexer api exception.
     *
     * @param message the message
     * @param responseStatusCode the response status ode
     */
    public NLPIndexerApiException(String message, int responseStatusCode) {
        super(message,responseStatusCode);
    }

    public NLPIndexerApiException(Exception e) {
        super(e);
    }
}
