package es.us.lsi.fogallego.nlpindexer.model.dataset;

import es.us.lsi.fogallego.nlpindexer.model.Message;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "datasetelementfull")
public class DataSetElementFull {

    @Id
    private String id;
    @Indexed
    private String idDataSet;
    private String originalText;
    private Message message;
    private List<MatchedRule> lstMatchedRule;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdDataSet() {
        return idDataSet;
    }

    public void setIdDataSet(String idDataSet) {
        this.idDataSet = idDataSet;
    }

    public String getOriginalText() {
        return originalText;
    }

    public void setOriginalText(String originalText) {
        this.originalText = originalText;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public List<MatchedRule> getLstMatchedRule() {
        return lstMatchedRule;
    }

    public void setLstMatchedRule(List<MatchedRule> lstMatchedRule) {
        this.lstMatchedRule = lstMatchedRule;
    }

}
