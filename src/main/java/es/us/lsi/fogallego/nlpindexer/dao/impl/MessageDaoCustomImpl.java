package es.us.lsi.fogallego.nlpindexer.dao.impl;

import au.com.bytecode.opencsv.CSVReader;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import es.us.lsi.fogallego.nlpindexer.correctors.TextCorrector;
import es.us.lsi.fogallego.nlpindexer.dao.MessageDaoCustom;
import es.us.lsi.fogallego.nlpindexer.idiomsrecognition.IdiomsRecognition;
import es.us.lsi.fogallego.nlpindexer.model.*;
import es.us.lsi.fogallego.nlpindexer.model.enums.EnumOwnTag;
import es.us.lsi.fogallego.nlpindexer.model.enums.EnumPreposition;
import es.us.lsi.fogallego.nlpindexer.patternextraction.UtilPatternExtraction;
import es.us.lsi.fogallego.nlpindexer.postagging.PosTagger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.tartarus.snowball.SnowballStemmer;
import org.tartarus.snowball.ext.spanishStemmer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

@Component
public class MessageDaoCustomImpl implements MessageDaoCustom {

    public static final String MODE_OWN = "own_tag";
    public static final String MODE_GROUPED = "grouped_own_tag";
    public static final String MODE_EAGLE = "eagle_tag";

    public static final String DOUBLE_HORIZONTAL_DELIMITER = "--";
    public static final String DOUBLE_VERTICAL_DELIMITER = "||";
    public static final String HORIZONTAL_DELIMITER = "-";
    public static final String VERTICAL_DELIMITER = "|";
    public static final String IDIOM_SUFIX = "_IDIOM";
    public static final String EMPTY = "";
    public static final String START_GROUP_TOKEN = "[";
    public static final String END_GROUP_TOKEN = "]";

    /** The log. */
    protected Logger log	= LoggerFactory.getLogger(this.getClass());

    Random rdm = new Random();

    /** The mongo template. */
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private TextCorrector textCorrector;

    @Autowired
    private PosTagger posTagger;
    private SnowballStemmer stemmer = new spanishStemmer();

    @Autowired
    private IdiomsRecognition idiomRecognition;

    @Override
    public Message indexMessageFromSentence(PosTaggedSentence sentence) {

        Message message = convert2Message(sentence);
        mongoTemplate.save(message);

        return message;
    }

    @Override
    public Message indexMessageFromSentence(PosTaggedSentence sentence, String idRawSentence) {

        Message message = convert2Message(sentence);
        message.setIdRawSentence(idRawSentence);
        mongoTemplate.save(message);

        return message;
    }

    @Override
    public void extractAndSaveFromFile(String fileIn) throws IOException {
        CSVReader csvReader = new CSVReader(new InputStreamReader(new FileInputStream(fileIn), "UTF-8"), ';');

        // Get all WordTokens
        List<String[]> lstArraySentence = csvReader.readAll();
        lstArraySentence.parallelStream()
                .map(l -> l.length == 2 ? new PosTaggedSentence(l[0], l[1]) : null)
                .filter(Objects::nonNull)
                .map(this::convert2Message)
                .forEach(message -> mongoTemplate.save(message));
        csvReader.close();
    }

    @Override
    public List<Message> getMatchedMessages(List<String> lstPatterns, Boolean ignoreInvalid) {

        Query query = getQueryForMatching(lstPatterns, ignoreInvalid);

        return mongoTemplate.find(query, Message.class);
    }

    @Override
    public List<Message> getSampleMatchedMessages(List<String> lstPatterns, Boolean ignoreInvalid, int sizeSample) {

        Query query = getQueryForMatching(lstPatterns, ignoreInvalid);
        int count = (int) mongoTemplate.count(query, Message.class);

        return count==0?
                new ArrayList<>():
                mongoTemplate.find(query.limit(sizeSample).skip(rdm.nextInt(count)), Message.class);
    }

    private Query getQueryForMatching(List<String> lstPatterns, Boolean ignoreInvalid) {
        String field;

        if (ignoreInvalid) {
            field = "nlpTag.patternWithInvalid";
        } else {
            field = "nlpTag.pattern";
        }

        DBObject queryCondition = new BasicDBObject();
        BasicDBList values = new BasicDBList();

        lstPatterns.forEach(
                pattern -> values.add(new BasicDBObject(field, new BasicDBObject("$regex", pattern)))
        );
        queryCondition.put("$or", values);
        return new BasicQuery(queryCondition);
    }

    @Override
    public Message convert2Message(PosTaggedSentence posTaggedSentence) {

        Message message = new Message();
        message.setText(posTaggedSentence.getText());
        List<LabelledWordParent> lstLabelledWord = new ArrayList<>();

        List<String> lstStems = posTaggedSentence.getLstWordToken()
                .stream()
                .map(wt -> stem(wt.getLemma()))
                .collect(Collectors.toList());

        List<Integer> lstIdiomsPosition = null;
        try {
            lstIdiomsPosition = idiomRecognition.getPositionsOfIdioms(lstStems);
        } catch (IOException e) {
            log.error("Idioms couln´t be annotated",e);
        }

        int position = 1;
        // Convert
        for (int i = 0; i < posTaggedSentence.getLstWordToken().size(); i++) {
            WordToken wt = posTaggedSentence.getLstWordToken().get(i);
            LabelledWordParent labelledWord = new LabelledWordParent();
            labelledWord.setToken(wt.getOriginal());
            labelledWord.setLemma(wt.getLemma());
            labelledWord.setEagle(wt.getTag());
            if (lstIdiomsPosition != null && lstIdiomsPosition.contains(i)) {
                labelledWord.setIdiom(true);
            }
            labelledWord.setOwnTag(UtilPatternExtraction.standardTag(wt.getLemma(), wt.getTag()));
            labelledWord.setPosition(position);
            position++;
            lstLabelledWord.add(labelledWord);
        }
        message.setLabelledWords(lstLabelledWord);

        // Group
        List<LabelledWordParent> lstGroupedLabelledWord = UtilPatternExtraction.groupLabelledWord(lstLabelledWord);
        message.setLabelledWordsGrouped(lstGroupedLabelledWord);

        NLPTag nlpTag = new NLPTag();
//        nlpTag.setEagle(String.join(HORIZONTAL_DELIMITER,lstLabelledWord.stream().map(LabelledWord::getEagle).collect(Collectors.toList())));
//        nlpTag.setOwn(String.join(HORIZONTAL_DELIMITER,lstLabelledWord.stream().map(lw -> lw.getOwnTag().name()).collect(Collectors.toList())));
//        nlpTag.setOwnGrouped(String.join(HORIZONTAL_DELIMITER,lstGroupedLabelledWord.stream().map(lw -> lw.getOwnTag().name()).collect(Collectors.toList())));
//        //idioms
//        nlpTag.setEagleIdiom(String.join(HORIZONTAL_DELIMITER,lstLabelledWord.stream().map(lw -> lw.getEagle() + (lw.isIdiom()? IDIOM_SUFIX : EMPTY)).collect(Collectors.toList())));
//        nlpTag.setOwnIdiom(String.join(HORIZONTAL_DELIMITER,lstLabelledWord.stream().map(lw -> lw.getOwnTag().name() + (lw.isIdiom()? IDIOM_SUFIX : EMPTY)).collect(Collectors.toList())));
//        nlpTag.setOwnGroupedIdiom(String.join(HORIZONTAL_DELIMITER,lstGroupedLabelledWord.stream().map(lw -> lw.getOwnTag().name() + (lw.isIdiom()? IDIOM_SUFIX : EMPTY)).collect(Collectors.toList())));

        //patterns
        nlpTag.setPattern(String.join(VERTICAL_DELIMITER,lstGroupedLabelledWord.stream().map(lw -> {
            List<LabelledWord> subLabelledWordList = lw.getSubLabelledWordList();
            String subWordPattern;
            if (subLabelledWordList != null && subLabelledWordList.size() > 0) {
                List<String> lstSubTokens = subLabelledWordList.stream().map(slw -> {
                    return getTokenPattern(slw);
                }).collect(Collectors.toList());
                subWordPattern = START_GROUP_TOKEN + String.join(HORIZONTAL_DELIMITER, lstSubTokens) + END_GROUP_TOKEN
                        + lw.getOwnTag();
            } else {
                subWordPattern = getTokenPattern(lw);
            }

            return subWordPattern;
        })
        .collect(Collectors.toList())));

        nlpTag.setPatternWithInvalid(String.join(VERTICAL_DELIMITER,lstGroupedLabelledWord.stream().map(lw -> {
            List<LabelledWord> subLabelledWordList = lw.getSubLabelledWordList();
            String subWordPattern;
            if (subLabelledWordList != null && subLabelledWordList.size() > 0) {
                List<String> lstSubTokens = subLabelledWordList.stream().map(slw -> {
                    if (slw.isIdiom()) {
                        return EnumOwnTag.INVALID.name();
                    } else {
                        return getTokenPattern(slw);
                    }
                }).collect(Collectors.toList());
                subWordPattern = START_GROUP_TOKEN + String.join(HORIZONTAL_DELIMITER, lstSubTokens) + END_GROUP_TOKEN
                        + lw.getOwnTag();
            } else {
                subWordPattern = lw.isIdiom()?EnumOwnTag.INVALID.name():getTokenPattern(lw);
            }

            return subWordPattern;
        })
        .collect(Collectors.toList())));

        message.setNlpTag(nlpTag);
        message.setAnnotatedText(
                String.join(DOUBLE_VERTICAL_DELIMITER, lstGroupedLabelledWord.stream()
                        .map(lw -> {
                            StringBuilder sb = new StringBuilder();
                            sb.append(lw.getPosition());
                            sb.append(DOUBLE_HORIZONTAL_DELIMITER);
                            sb.append(lw.getToken());
                            sb.append(DOUBLE_HORIZONTAL_DELIMITER);
                            sb.append(lw.getOwnTag());

                            return sb.toString();
                        })
                        .collect(Collectors.toList()))
        );

        return message;
    }

    private String getTokenPattern(LabelledWord slw) {

        String result = slw.getOwnTag().name();

        if (slw.getOwnTag().equals(EnumOwnTag.PREP)) {
            switch (slw.getLemma()) {
                case "de":
                    result += "(" + EnumPreposition.OF.name() + ")";
                    break;
                case "con":
                    result += "(" + EnumPreposition.WITH.name() + ")";
                    break;
                case "a":
                    result += "(" + EnumPreposition.AT.name() + ")";
                    break;
                case "en":
                    result += "(" + EnumPreposition.IN.name() + ")";
                    break;
                case "desde":
                    result += "(" + EnumPreposition.FROM.name() + ")";
                    break;
                case "bajo":
                    result += "(" + EnumPreposition.UNDER.name() + ")";
                    break;
                case "para":
                    result += "(" + EnumPreposition.FOR.name() + ")";
                    break;
                case "hasta":
                    result += "(" + EnumPreposition.UNDER.name() + ")";
                    break;
                default:
                    result += "(" + EnumPreposition.OTHER.name() + ")";
                    break;
            }
        }

        return result;
    }

    private synchronized String stem(String token) {

        stemmer.setCurrent(token);
        stemmer.stem();
        stemmer.stem();
        stemmer.stem();

        return stemmer.getCurrent();
    }
}
