package es.us.lsi.fogallego.nlpindexer.dto.search.output;

import es.us.lsi.fogallego.nlpindexer.dto.search.SearchOutputDTO;

public class SearchOutputDatasetDTO extends SearchOutputDTO {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
