package es.us.lsi.fogallego.nlpindexer.controllers;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import es.us.lsi.fogallego.nlpindexer.correctors.TextCorrector;
import es.us.lsi.fogallego.nlpindexer.dao.MessageDaoCustom;
import es.us.lsi.fogallego.nlpindexer.dao.RawSentenceDao;
import es.us.lsi.fogallego.nlpindexer.dao.RawTextDao;
import es.us.lsi.fogallego.nlpindexer.exceptions.NLPIndexerApiException;
import es.us.lsi.fogallego.nlpindexer.languagedetector.LanguageDetectorAdapter;
import es.us.lsi.fogallego.nlpindexer.model.PosTaggedSentence;
import es.us.lsi.fogallego.nlpindexer.model.RawSentence;
import es.us.lsi.fogallego.nlpindexer.model.RawText;
import es.us.lsi.fogallego.nlpindexer.postagging.PosTagger;
import org.apache.catalina.connector.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Api("rawtext")
@RestController
@RequestMapping(PathVariables.API + "rawtext")
public class RawTextController {

    private static final int RPP = 500;
    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MessageDaoCustom messageDaoCustom;

    @Autowired
    private RawTextDao rawTextDao;

    @Autowired
    private RawSentenceDao rawSentenceDao;

    @Autowired
    private LanguageDetectorAdapter lAdapter;

    @Autowired
    private TextCorrector textCorrector;

    @Autowired
    private PosTagger posTagger;

    @Autowired
    private MongoTemplate mongoTemplate;

    @ApiOperation(value = "Index from file with NLP annotations")
    @ApiResponses({@ApiResponse(code = Response.SC_BAD_REQUEST, message = "IOException while processing file")})
    @RequestMapping(value = "/importer", method = RequestMethod.POST)
    public Boolean indexFromFile(@RequestParam String filePath, @RequestParam Boolean createMessage) throws NLPIndexerApiException {

        log.warn("Start indexing raw text from file: " + filePath);

        mongoTemplate.remove(new Query(), "message");
        mongoTemplate.remove(new Query(), "dataset");
        mongoTemplate.remove(new Query(), "datasetelementfull");
        mongoTemplate.remove(new Query(), "datasetelementlite");
        mongoTemplate.remove(new Query(), "rawsentence");
        mongoTemplate.remove(new Query(), "rawtext");

        try {
            Files.lines(Paths.get(filePath))
                    .forEach(text -> {
                        String textCleaned = text.replaceAll("\\p{Cntrl}", "");

                        if (textCleaned.isEmpty()) {
                            log.error("Problem of empty text: "+text);
                        } else {
                            textCleaned = textCorrector.fixEndOfText(textCleaned);

                            boolean majorityUpperCase = textCorrector.hasMajorityUpperCase(textCleaned);
                            boolean probablySpanish = lAdapter.isProbablySpanish(textCleaned.toLowerCase());

                            if (!majorityUpperCase && probablySpanish) {
                                try {
                                    RawText rawText = new RawText();
                                    rawText.setText(textCleaned);

                                    if (rawTextDao.findByTextDigest(rawText.getTextDigest()) == null) {
                                        String corrected = textCorrector.correctText(textCleaned);
                                        rawText.setCorrected(corrected);

                                        final RawText savedRawText = rawTextDao.insert(rawText);

                                        generateRawSentences(createMessage, savedRawText);
                                    } else {
                                        log.warn("This text already exists in db: " + text);
                                    }

                                } catch (NLPIndexerApiException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    });
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new NLPIndexerApiException(e);
        }

        return true;
    }

    @RequestMapping(value = "/regenerate", method = RequestMethod.POST)
    public Boolean regenerate(@RequestParam Boolean createMessage) throws NLPIndexerApiException {

        mongoTemplate.remove(new Query(), "message");
        mongoTemplate.remove(new Query(), "dataset");
        mongoTemplate.remove(new Query(), "datasetelementfull");
        mongoTemplate.remove(new Query(), "datasetelementlite");
        mongoTemplate.remove(new Query(), "rawsentence");

        int page = 0;

        Pageable pageable = new PageRequest(page,RPP);

        List<RawText> lstRawText;
        do {
            lstRawText = rawTextDao.findAll(pageable).getContent();
            for (RawText rawText : lstRawText) {
                String corrected = textCorrector.correctText(rawText.getText());
                rawText.setCorrected(corrected);
                rawTextDao.save(rawText);
                generateRawSentences(createMessage,rawText);
            }
            page++;
        } while (lstRawText != null && lstRawText.size() > 0);

        return true;
    }

    private void generateRawSentences(Boolean createMessage, RawText rawText) throws NLPIndexerApiException {
        List<String> rawSentences = posTagger.getRawSentences(rawText.getText());
        for (String sent : rawSentences) {
            RawSentence rawSentence = new RawSentence();
            rawSentence.setText(sent);
            rawSentence.setCorrected(textCorrector.correctText(sent));
            rawSentence.setIdText(rawText.getId());

            rawSentence = rawSentenceDao.insert(rawSentence);
            if (createMessage) {
                processAndSaveFromRaw(rawSentence);
            }
        }
    }

    private void processAndSaveFromRaw(final RawSentence rawSentence) {
        List<PosTaggedSentence> lstSentence = posTagger.getSentences(rawSentence.getCorrected());
        lstSentence.forEach(sent -> messageDaoCustom.indexMessageFromSentence(sent, rawSentence.getId()));
    }

}
