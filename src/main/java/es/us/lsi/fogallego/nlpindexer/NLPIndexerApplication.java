package es.us.lsi.fogallego.nlpindexer;

import es.us.lsi.fogallego.nlpindexer.controllers.PathVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger1.annotations.EnableSwagger;

/**
 * The Class NLPIndexerServiceApplication.
 */
@SpringBootApplication
@EnableSwagger
@ComponentScan
@EnableMongoAuditing
public class NLPIndexerApplication {

    /**
     * The log.
     */
    protected static Logger log = LoggerFactory.getLogger(NLPIndexerApplication.class);

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        log.info("Starting NLPIndexer");
        SpringApplication.run(NLPIndexerApplication.class, args);
        log.info("NLPIndexer started");
    }

    /**
     * nlpindexerAPI
     *
     * @return the docket
     */
    @Bean
    public Docket nlpindexerAPI() {
        ApiInfo apiInfo = new ApiInfoBuilder().title("NLPIndexer service")
                .description("This services offers indexing and querying for NLP data. All API endpoints require BASIC Authentication").contact("fogallego@us.es")
                .version("0.0.1-alpha").build();

        return new Docket(DocumentationType.SWAGGER_12).groupName("nlp-indexer").apiInfo(apiInfo).useDefaultResponseMessages(false).select().paths(PathSelectors.ant(PathVariables.API + "**"))
                .build();
    }
}
