package es.us.lsi.fogallego.nlpindexer.postagging;

import edu.upc.freeling.*;
import es.us.lsi.fogallego.nlpindexer.model.PosTaggedSentence;
import es.us.lsi.fogallego.nlpindexer.model.WordToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PosTagger {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    // Modify this line to be your FreeLing installation directory
    @Value("${postagging.freeling.dir}")
    private String FREELINGDIR;
    @Value("${postagging.freeling.data}")
    private String DATA;
    @Value("${postagging.freeling.lang}")
    private String LANG;
    @Value("${postagging.freeling.libraryPath}")
    private String LIBRARYPATH;
    private Tokenizer tk;
    private Splitter sp;
    private Maco mf;

    private HmmTagger tg;
    private ChartParser parser;
    private DepTxala dep;
    private Nec neclass;

    private Senses sen; // sense dictionary
    private Ukb dis; // sense disambiguator

    private boolean loaded = false;

    private void loadFreeling() {

        if (!loaded) {

            String osName = System.getProperty("os.name");
            String libname;
            if (osName.contains("Windows")) {
                libname = LIBRARYPATH;
                System.loadLibrary(libname);
            } else {
                libname = LIBRARYPATH + ".so";
                System.load(libname);
            }
//        System.load(LIBRARYPATH);

            Util.initLocale("es");

            // Create options set for maco analyzer.
            // Default values are Ok, except for data files.
            MacoOptions op = new MacoOptions(LANG);

            op.setActiveModules(true, true, true, false, true, false,
                    false, true, true, true, false);

            op.setDataFiles(
                    DATA + LANG + "\\usermap.dat",
                    DATA + LANG + "\\locucions.dat",
                    DATA + LANG + "\\quantities.dat",
                    DATA + LANG + "\\afixos.dat",
                    DATA + LANG + "\\probabilitats.dat",
                    DATA + LANG + "\\dicc.src",
                    DATA + LANG + "\\np.dat",
                    DATA + "common\\punct.dat");

            tk = new Tokenizer(DATA + LANG + "\\tokenizer.dat");
            sp = new Splitter(DATA + LANG + "\\splitter.dat");
            mf = new Maco(op);

            tg = new HmmTagger(DATA + LANG + "\\tagger.dat", true, 2);
            parser = new ChartParser(
                    DATA + LANG + "\\chunker\\grammar-chunk.dat");
            dep = new DepTxala(DATA + LANG + "\\dep\\dependences.dat",
                    parser.getStartSymbol());
            neclass = new Nec(DATA + LANG + "\\nerc\\nec\\nec-ab-poor1.dat");

            sen = new Senses(DATA + LANG + "\\senses.dat"); // sense dictionary
            dis = new Ukb(DATA + LANG + "\\ukb.dat"); // sense disambiguator

            loaded = true;
        }
    }

    public List<String> getRawSentences(String inText) {

        List<String> lstRawSentences = new ArrayList<>();

        log.debug("Splitting text: " + inText);

        loadFreeling();

        // Extract the tokens from the line of text.
        ListWord l = tk.tokenize(inText);

        // Split the tokens into distinct sentences.
        ListSentence ls = sp.split(l, false);

        ListSentenceIterator sIt = new ListSentenceIterator(ls);
        while (sIt.hasNext()) {
            edu.upc.freeling.Sentence s = sIt.next();
            VectorWord words = s.getWords();
            List<String> lstWords = new ArrayList<>();
            for (int i=0;i<words.size();i++) {
                lstWords.add(words.get(i).getForm());
            }

            lstRawSentences.add(String.join(" ", lstWords));
        }

        ls.clear();
        l.clear();

        return lstRawSentences;
    }

    /**
     * Get the sentences with the postag annotation corresponding to sentences of inText
     *
     * @param inText the text to be processed
     * @return the list of postagged sentences of inText
     */
    public List<PosTaggedSentence> getSentences(String inText) {

        log.debug("Processing text: " + inText);

        loadFreeling();

        List<PosTaggedSentence> lstPosTaggedSentence = new ArrayList<>();

        // Extract the tokens from the line of text.
        ListWord l = tk.tokenize(inText);

        // Split the tokens into distinct sentences.
        ListSentence ls = sp.split(l, false);

        // Perform morphological analysis
        mf.analyze(ls);

        // Perform part-of-speech tagging.
        tg.analyze(ls);

        // Perform named entity (NE) classification.
        neclass.analyze(ls);

        sen.analyze(ls);
        dis.analyze(ls);

        // get the analyzed words out of ls. (TODO: update to Java 8 style)
        ListSentenceIterator sIt = new ListSentenceIterator(ls);
        while (sIt.hasNext()) {
            edu.upc.freeling.Sentence s = sIt.next();
            ListWordIterator wIt = new ListWordIterator(s);
            List<WordToken> lstWordToken = new ArrayList<>();
            while (wIt.hasNext()) {
                Word w = wIt.next();

                lstWordToken.add(new WordToken(w.getForm(), w.getLemma(), w.getTag()));
            }
            List<String> lstSentenceString = new ArrayList<>();
            VectorWord vw = s.getWords();
            for (int i = 0; i < vw.size(); i++) {
                lstSentenceString.add(vw.get(i).getForm());
            }
            String sentenceString = String.join(" ", lstSentenceString);
            lstPosTaggedSentence.add(new PosTaggedSentence(sentenceString, lstWordToken));
        }

        log.debug("Processing finished!");

        l.clear();
        ls.clear();

        return lstPosTaggedSentence;
    }
}
