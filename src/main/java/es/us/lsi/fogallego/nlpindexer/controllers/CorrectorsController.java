package es.us.lsi.fogallego.nlpindexer.controllers;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import es.us.lsi.fogallego.nlpindexer.correctors.TextCorrector;
import es.us.lsi.fogallego.nlpindexer.exceptions.NLPIndexerApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api("corrector")
@RestController
@RequestMapping(PathVariables.API+"corrector")
public class CorrectorsController {

    protected Logger log	= LoggerFactory.getLogger(this.getClass());

    @Autowired
    TextCorrector textCorrector;

    @ApiOperation(value = "Correct text")
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    String correctText(@RequestParam(value = "inputText") String inputText) throws NLPIndexerApiException {

        try {
            return textCorrector.correctText(inputText);
        } catch (Exception e) {
            throw new NLPIndexerApiException(e);
        }
    }

}
