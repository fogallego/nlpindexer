import org.tartarus.snowball.ext.spanishStemmer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Convert2Stem {

    public static void main(String[] args) throws IOException {

        Files.write(Paths.get("out_stems"), Files.lines(Paths.get("in_stems_es"))
            .map(s -> {
                return String.join(" ",Arrays.asList(s.split(" "))
                    .stream()
                    .map(s2 -> stem(s2))
                    .collect(Collectors.toList()));
            })
            .collect(Collectors.toList()));
    }

    private static String stem(String string) {
        org.tartarus.snowball.SnowballStemmer stemmer = new spanishStemmer();
        stemmer.setCurrent(string);
        stemmer.stem();
        stemmer.stem();
        stemmer.stem();

        return stemmer.getCurrent();
    }

}
